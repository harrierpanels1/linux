<pre>
    ====================== Linux Task 1 Multi-Tool ======================

Usage: ./task01.sh [OPTIONS]
Examples: ./task01.sh -t 1 file; ./task01.sh --task 6 dir1 dir2 cksum;

Script features:
-----------------
The script performs up to 43 Bash & Linux related tasks without using 'sed' or 'awk'.
It has the following features: Demo Mode, Scripting, Pipes, and Chaining.

[d] Demo Mode:
-----------------
This feature is implemented for testing purposes. It utilizes the SandBox functions to
create a temporary environment that contains the necessary files to test each task's
performance. The results of each task will be displayed in the console.

To start Demo Mode, run the following command:
./task01.sh -t -d /path/to/directory or ./task01.sh -t --demo /path/to/directory

The SandBox functions will first download the necessary archives and extract them to
the folder you specify. Then, each task will be run one by one, starting from the first,
and the output for each task will be displayed until Demo Mode reaches the final task.
After that, all temporary content will be removed.

IMPORTANT: Make sure you have an active internet connection so that the SandBox functions
can download the necessary archives. Specify a directory for Demo Mode that does not
already exist. The SandBox functions will create it for you.

Scripting:
-----------------
The script is highly flexible and supports various scenarios, including scripting.
If you need to pass a list of options such as directories or files as arguments on the
command line, there are multiple ways to do it!

For example, in Task 18, you can pass files as arguments with the last argument being
a directory. One way is to use the 'echo' command to pass the arguments in a variable:

./task01.sh -t 18 $(echo "path/to/file1 path/to/dir4/file2 path/to/dir5")

This will pass three arguments to the script: a list of files (file1 and file2) and
a directory (dir5) with a relative path. Here's another example using scripting in the
command line:

./task01.sh -t 18 $(for file in $(ls -h path/to/dir4/dir5); \
do \
    echo "path/to/dir4/dir5/$file"; \
done) path/to/dir4

This will pass all files with a relative path from the directory path/to/dir4/dir5
as arguments to the script. Similarly, you can use a 'while' loop with process
substitution:

args="" && while read file; do \
    args="$args path/to/dir/with/files/$file"; \
done < <(ls -h path/to/dir/with/files) &&
./task01.sh -t 18 $args path/to/dir

This will assign a list of files to the 'args' variable and pass them as arguments to
the script. Another approach is to use a 'for' loop:

args="" &&
for file in path/to/dir/with/files/*; do \
    args="$args $file"; \
done &&
./task01.sh -t 18 $args path/to/dir

In this case, the for loop iterates over the files in the specified directory, and
the 'args' variable is constructed with the file paths. Finally, the 'args' variable is
passed as arguments to the script.

If you have a file with a list of files, you can use the 'cat' command to pass them as
arguments:

./task01.sh -t 18 $(cat path/to/file) path/to/dir

This will pass a list of files as arguments to the script, reading them from the
specified file. Alternatively, you can use a 'while' loop with input redirection:

while read file; do \
    ./task01.sh -t 18 $file path/to/dir; \
done < path/to/file

This will pass a list of files as arguments to the script, reading them from the file
specified in the input redirection.

Pipes:
-----------------
Using pipes adds even more flexibility and functionality to the script. Let's explore
the same scenarios mentioned in the "Scripting" section. For example, if we need to pass
files to the script as required in Task 18, we can use the following approach:

args=$(ls -h path/to/dir/with/files |
    while read -r file; do \
        echo "path/to/dir/with/files/$file"; \
    done) &&
./task01.sh -t 18 $args path/to/dir

In this example, the 'args' variable is passed with a list of files as arguments to
the script. If you have a file with a list of files, and the last entry in the list is
a directory, you can use the 'cat' command in a pipe:

cat path/to/file | xargs ./task01.sh -t 18

This will pass a list of files as arguments to the script, reading them from the file
specified with 'cat'. You can also use 'xargs' and the 'cat' command to process the file:

cat path/to/file | xargs -I {} ./task01.sh -t 18 {} path/to/dir

This approach uses 'xargs' with the -I option to specify a placeholder {} for each
argument. Another option is to use 'xargs' with the output of the 'ls' command for a
directory with files:

ls -1 path/to/dir/with/files |
xargs -I {} ./task01.sh -t 18 path/to/dir/with/files/{} path/to/dir

This will pass the file paths as arguments to the script. Lastly, you can achieve the
same result using the 'cat' command and a 'for' loop in a variable:

ls -1 path/to/dir/with/files |
./task01.sh -t 18 $(for file in $(cat); do \
    echo path/to/dir/with/files/$file; \
done) path/to/dir

This approach uses a 'for' loop to construct the file paths, which are then passed as
arguments to the script. If you want to pass arguments from a file using the 'cat'
command:

cat path/to/file | ./task01.sh -t 18 $(cat) path/to/dir

Feel free to experiment with different options and combinations, including scripting,
piping, and chaining, to explore different subsets of code based on your requirements.

Chaining:
-----------------
The script supports task chaining. That means you can specify as many tasks in the
command line as you like with their options accordingly, and they will be executed
in the order you specify.

Ensure that you provide the exact number of arguments required by each task when
chaining commands together. Refer to the examples provided for guidance.

IMPORTANT: Tasks that are marked with [x] should be run at the end of the chain! One
task marked with [x] per chain only! If you need to run more than one task marked with
[x] use && to link another instance of the script with the task marked with [x].

Examples:
./task01.sh -t 2 dir ec2-user ec2-user -t 3 dir -t 1 file -t 6 dir1 dir2 cksum;
./task01.sh -t 15 -t 6 dir2 cksum && ./task01.sh -t 18 dir/file1 dir/file2 dir5;

  Options:

 -t, --task TASK_NUMBER    A mandatory option followed by a task number

  1) List all system groups and retrieve their unique names and IDs. Save the output
     to a file.

     This task involves retrieving a list of all system groups on the Linux system and
     extracting their unique names and corresponding IDs. The output of the command will
     be saved to a file for further reference or analysis.

     NOTE: If the specified file directory does not exist, the script will create it.

     Example: ./task01.sh -t 1 file;

  2) Find all files and directories that have corresponding user and group permissions
     in a specified directory.

     This task involves searching for files and directories within a specified directory
     that have matching user and group permissions. It helps identify the files and
     directories where the user and group ownership aligns with the specified permissions.

     Example: ./task01.sh -t 2 dir ec2-user ec2-user;

  3) Find all scripts in a specified directory and its subdirectories.

     This task aims to locate all script files within a specific directory and its
     subdirectories. It searches for files with specific file extensions (e.g., .sh, .py,
     .rb) that are commonly associated with script files. By traversing the directory
     tree, it identifies and lists all script files found, including their paths.

     This enables easy access and management of script files within the specified
     directory and its subdirectories.

     NOTE: The specific file extensions for scripts can vary based on the programming or
     scripting languages used.

     Example: ./task01.sh -t 3 dir;

  4) Search for script files by a specific user in a specified directory.

     This task searches for script files within a specified directory that are owned by
     a specific user. It helps to identify and locate script files that belong to a
     particular user within the given directory.

     Example: ./task01.sh -t 4 /home/ec2-user/dir ec2-user;

  5) Perform a recursive (case-sensitive) word or phrase search for a specific file type
     in a specified directory.

     This task involves conducting a recursive search for a specific file type within a
     specified directory. It includes the additional requirement of performing a
     case-sensitive search for a particular word or phrase within the files of that file
     type.

     The search aims to identify and locate files that contain the desired word or phrase,
     enabling targeted analysis or investigation within the specified directory.

     IMPORTANT: To ensure proper functioning of the script, it is important to specify
     the file extension correctly. The script has a logic to handle the file extension,
     so it is recommended to use a dot followed by the file extension (e.g. .txt) or the
     extension without the dot (e.g. txt).

     NOTE: If you need to use wildcards (e.g. *txt or *.txt), it is recommended to
     enclose them in single or double quotes (e.g. '*txt' or '*.txt'). This will ensure
     that the script interprets the wildcard correctly and prevents unexpected behavior.

     Example: ./task01.sh -t 5 dir sh bash;

  6) [x] Find duplicate files in specified directories. First compare by size, then by
     option (choose a hash function: CRC32 command: cksum; MD5 command: md5sum; SHA-1
     command: sha1sum; SHA-224 command: sha224sum). The result should be sorted by
     filename.

     NOTE: In this task, you can specify multiple directories by using the following
     format: -t dir1 dir2 ... dir(n) hash_function_command. You can include as many
     directories as needed, followed by the hash function command.

     IMPORTANT: The task should be run at the end of the chain! See more in the
     'Chaining' section.

     Example: ./task01.sh -t 6 dir1 dir2 cksum;

  7) Find all symbolic links to a file by its name and path.

     IMPORTANT: Remember that when using the option you should always use the path
     to the target file as it appears in the symbolic link, whether it is an absolute
     or relative path.

     TIP: If you don't know what path is specified in the symbolic link use an absolute
     or relative path to the file and the script will try to handle it in the best way
     possible.

     Examples:
     ./task01.sh -t 7 dir2 /home/ec2-user/dir/file;
     ./task01.sh -t 7 dir2 dir/file;

  8) Find all hard links to a file by its name and path.

     This task involves searching for all hard links to a specific file based on its name
     and path. Hard links are multiple entries in the file system that point to the same
     underlying file data. By using the name and path of the file as search criteria, you
     can identify all other hard links that share the same file data.

     Example: ./task01.sh -t 8 dir2 dir/file;

  9) Given only the inode of a file, find all its names.

     This task involves finding all the names associated with a file based on its inode.
     The inode is a data structure in a file system that stores metadata about a file,
     including its unique identifier.

     By using the inode number, this task aims to identify all the names by which the
     file is referenced within the file system.

     Example: ./task01.sh -t 9 33721754;

 10) Given only the inode of a file, find all its names taking into account that there
     may be multiple mounted sections.

     This task extends the previous one by considering the possibility of multiple
     mounted sections. A mounted section refers to a separate file system that is mounted
     at a specific location in the directory tree.

     When a file is present in multiple mounted sections, it can have different names
     associated with its inode in each section. Therefore, this task aims to identify all
     the names associated with the given inode, considering the presence of multiple
     mounted sections.

     Example: ./task01.sh -t 10 33721754;

 11) Correctly delete a file, taking into account the possibility of the existence
     of symbolic or hard file links.

     This task involves deleting a file while considering the presence of symbolic or
     hard file links. Deleting a file with links requires careful handling to ensure that
     all associated links are correctly updated or removed to maintain data integrity.

     By accounting for both symbolic and hard file links, the deletion process ensures
     that all related references to the file are appropriately handled to prevent any
     issues with link consistency or orphaned links.

     IMPORTANT: Use the task with caution as it may delete symbolic links that point to
     another file with the same name as the specified file. Please ensure that you only
     use this task when you are certain that you want to delete all files and links that
     match the specified file.

     Example: ./task01.sh -t 11 dir/delete.txt;

 12) Recursively change access rights to files (specified by file mask) in a specified
     directory.

     This task involves modifying the access rights (permissions) of files within a
     specified directory. It applies to files that match a specified file mask or pattern.

     The access rights can include permissions such as read, write, and execute for the
     owner, group, and others. By recursively changing the access rights, it affects all
     files within the directory and its subdirectories that match the specified file mask.

     NOTE: The specific access rights to be modified and the file mask pattern should be
     provided as arguments when executing the task.

     IMPORTANT: Make sure you specify a file mask in quotes! Refer to the examples
     below for guidance.

     Examples:
     ./task01.sh -t 12 dir3 '*.txt' 644;
     ./task01.sh -t 12 dir3 "*t" u+x,o+w;

 13) Compare two directories recursively and display only the files that differ. For
     each file that differs, print two lines of output, and starting from the third line,
     print the lines that differ between the two files.

     This task involves comparing the contents of two directories, including all
     subdirectories, and identifying the files that have differences between them.
     The output will show only the files that differ, allowing for easy identification of
     the differing files in the compared directories.

     Example: ./task01.sh -t 13 dir3 dir4;

 14) Get MAC addresses of network interfaces.

     This task involves retrieving the MAC addresses (Media Access Control addresses) of
     the network interfaces on a system. MAC addresses are unique identifiers assigned to
     network devices, and obtaining them can be useful for various network-related
     operations and configurations.

     Example: ./task01.sh -t 14;

 15) Display the list of users currently authorized in the system.

     This task aims to provide a brief description of displaying the list of users who
     are currently authorized or logged in to the system. It helps in obtaining an
     overview of the active user sessions or logged-in users on the system.

     Example: ./task01.sh -t 15;

 16) Display a list of active network connections in the form of a table:
     connection type and the number of connections.

     This task aims to provide a clear and organized view of the current active network
     connections on the system. The output will be formatted as a table, displaying
     relevant information about each network connection. This includes details such as
     the connection type, status, and associated endpoints (source and destination).

     By presenting the network connections in a table format, it becomes easier to
     analyze and understand the active network connections at a glance. This can be
     useful for monitoring network activity, identifying established connections, and
     troubleshooting network-related issues.

     Example: ./task01.sh -t 16;

 17) Re-assign an existing symbolic link.

     This task involves changing the target of an existing symbolic link to point to
     a different file or directory. It allows you to update the reference of the symbolic
     link to a new location while preserving the link itself. The original link remains
     intact, but its destination is modified to the specified target.

     Note that if the existing link points to a file or directory that has been
     deleted or moved, the link will be broken and the script will create a new
     link that points to the new target.

     Example: ./task01.sh -t 17 script.py dir4/script_soft.py;

 18) [x] Create symbolic links to multiple files specified by their relative paths,
     using a directory path as the location for the symbolic links.

     NOTE: In this task, you can specify multiple files using the following format:
     -t file1 file2 ... file(n) dir. You can include as many files as needed, followed
     by a path to the directory in which the symbolic links to the files will be created.

     If you specify an absolute path to a file (e.g. /home/user/path/to/file), don't
     worry! The script will still create a relative symbolic link. You can also specify
     either relative or absolute path to a directory.

     NOTE: If the specified directory does not exist, the script will create it. If you
     specify a system path for a destination directory (e.g., /tmp/dir),the script will
     create relative symbolic links for all files within the directory. Similarly, if
     you specify a system path for a file, the script will create a relative symbolic
     link to the file.

     NOTE: If you specify a directory that is the root directory of the file you want to
     create a symbolic link for, no link will be created for the file. This is because
     the script creates symbolic links with the same name as the original file specified.
     However, you will see a message indicating that a link was created, even though no
     link was actually created.

     If you have a file with a list of files, you can use the 'cat' command to pass them
     as arguments to the script on the command line:

     ./task01.sh -t 18 $(cat path/to/file) path/to/dir

     Refer to the 'Scripting' and 'Pipes' section of the manual for more information.

     IMPORTANT: The task should be run at the end of the chain! See more in the
     'Chaining' section.

     Examples:
     ./task01.sh -t 18 /home/ec2-user/dir4/hi.txt dir3/script2.py dir5/d/e;
     ./task01.sh -t 18 dir4/hi.txt dir3/script2.py /tmp;
     ./task01.sh -t 18 dir4/hi.txt /tmp/file.tmp /home/ec2-user/dir5;

 19) Copy a directory containing hard links, direct and relative symbolic links,
     preserving attributes and permissions, for backup on removable storage. Provide
     two versions using 'cp' and 'rsync' commands.

     This option allows you to copy a directory or its contents while preserving soft
     and hard links, ownership, permissions, and attributes for backup purposes. Two
     versions are provided using the 'cp' and 'rsync' commands.

     To use this option, specify the command you want to use ('cp' or 'rsync') followed
     by the source directory and the destination directory.

     If the destination directory does not exist, the script will create it for you.
     If you want to copy the directory's contents only, specify it as 'dir/.' with a
     relative or absolute path to the directory.

     IMPORTANT: Do not use wildcards like 'dir/*' if you want to copy the contents only.
     Instead, use 'dir/.'

     Examples:
     ./task01.sh -t 19 cp /tmp/dir dir5;
     ./task01.sh -t 19 rsync dir4/. /tmp;

 20) Copy the directory, taking into account that it contains hard and soft links to
     files, both direct and relative.

     This option is similar to Task 19 but less strict, preserving only the hard and
     soft links. It creates the destination directory if it doesn't exist. You can use
     either 'cp' or 'rsync' to copy the entire directory or just its contents if
     specified as 'dir/.'.

     IMPORTANT: Do not use wildcards like 'dir/*' if you want to copy only the contents.
     Instead, use 'dir/.'

     Examples:
     ./task01.sh -t 20 cp dir dir5;
     ./task01.sh -t 20 rsync dir4/. ~/dir5/dir;

 21) Copy all files and directories from the specified directory to a new location
     while preserving attributes and permissions.

     This option is similar to Tasks 19-20 with only requirement being to preserve
     permissions and attributes. It creates the destination directory if it doesn't
     exist. You can use either 'cp' or 'rsync' to copy the entire directory or just
     its contents if specified as 'dir/.'.

     IMPORTANT: Do not use wildcards like 'dir/*' if you want to copy only the contents.
     Instead, use 'dir/.'

     Examples:
     ./task01.sh -t 21 cp dir/. dir5;
     ./task01.sh -t 21 rsync ~/dir4 dir5/dir;

 22) In the project directory, convert all relative links to direct links.

     This task aims to convert all relative links within the project directory to direct
     links. By doing so, the links will point directly to the absolute path of their
     targets.

     This conversion ensures that the links remain valid and functional, even if the
     project directory or the referenced files are moved or accessed from different
     locations.

     NOTE: It's essential to exercise caution when performing this conversion, as it
     alters the behavior of the links within the project directory.

     Example:
     ./task01.sh -t 22 dir14;

 23) In the project directory, convert all direct links to relative links to the project
     directory.

     This task aims to convert all direct symbolic links within the project directory to
     relative links referencing the project directory. By doing so, the links will point
     to the target file or directory using a relative path from the project directory.

     This conversion ensures that the links remain valid and maintain their functionality
     even when the project directory is moved or accessed from different locations. It
     allows for easier portability and flexibility when working with symbolic links
     within the project.

     NOTE: If the conversion encounters any issues with the 'realpath' command, please
     ensure that the required files and directories exist and that the script has
     appropriate permissions to access them.

     Example:
     ./task01.sh -t 23 dir14;

 24) Find all broken links in the specified directory and delete them.

     This task aims to identify and remove broken symbolic links within the specified
     directory and its subdirectories. Broken links are symbolic links that point to
     non-existent targets.

     The command uses the 'find' utility to search for broken links using the '-xtype l'
     option. Once found, the links are deleted using the '-delete' option. Additionally,
     the command searches for subdirectories ('-type d') and recursively deletes broken
     links within each subdirectory.

     Note: Exercise caution when using this command as it permanently deletes broken
     symbolic links.

     Example:
     ./task01.sh -t 24 dir15;

 25) [x] Unpack all contents or multiple directories or files from an archive to a
     specified location.

     This task allows you to unpack a specific directory or file from different archive
     formats to the destination directory. The supported archive formats include:
     - *.tar.gz, *.tgz, *.tar.Z
     - *.tar.bz2, *.tbz2, *.tlz
     - *.tar.lzma, *.tlzma, *.tar.xz, *.txz
     - *.tar, *.tar.lz

     The files will be unpacked using the 'tar' command. Please make sure you have the
     necessary compression utilities installed for the corresponding archive formats.

     IMPORTANT: To use Task 25 options, you need to install 'compress' or 'ncompress'
     for '.Z' archives, 'Lzip' for '.lz' and '.lzma' archives and 'bzip2' executable,
     which is used to handle '.bz2'.

     Please ensure that you provide the arguments in the correct order to extract the
     content accurately. For each archive format, consider the compression options
     applicable to extract the content correctly.

     To unpack the desired content, you need to specify the arguments in the following
     order:
     ./task01.sh -t 25 path/to/archive path/to/dir strip-option file1 .. file(n)

     where 'path/to/dir' is the destination directory, strip-option - the number of
     parent directories to skip when extracting (see more below), file or files and/or
     directories to extract from the archive. The script supports extracting multiple
     files or directories within the archive or just a single one.

     NOTE: If the destination directory does not exist, the script will create it for you.

     The strip-option:
     The strip-option determines how many leading components of the directory structure
     should be stripped during extraction. For example, specifying '--strip-components=1'
     will remove the top-level directory from the extracted contents.

     This option is useful when you want to extract only specific subdirectories or files
     without including the parent directory structure.

     NOTE: If the specified strip-option value exceeds the number of leading components
     in the archive, no files will be extracted.

     IMPORTANT: If you want to keep the parent directory structure as is you should
     specify the strip-option as '0'. For example:
     ./task01.sh -t 25 path/to/archive path/to/dir 0 dir1 file1 .. dir(n) file(n)

     '--all' feature:
     The '--all' feature allows you to extract all contents from the archive. For example:
     ./task01.sh -t 25 path/to/archive path/to/dir 1 --all

     In this example, the strip-option is set to 1, which means it will skip one parent
     directory when extracting all contents.

     IMPORTANT: Ensure that you run this task at the end of the chain when using the
     script in a chain of tasks. Refer to the 'Chaining' section for more details.

     Examples:
     ./task01.sh -t 25 dir17/archive10.tgz dir16 0 --all;
     ./task01.sh -t 25 ~/dir17/archive11.tlzma dir17 1 a;
     ./task01.sh -t 25 dir17/archive12.txz dir16 1 a/2.txt;
     ./task01.sh -t 25 dir17/archive1.tar dir17 0 3.txt 1.txt 4.txt a/2.txt;
     ./task01.sh -t 25 dir17/archive2.tar.gz dir16 0 3.txt a;
     ./task01.sh -t 25 dir17/archive3.tar.bz2 dir16 0 1.txt;
     ./task01.sh -t 25 dir17/archive4.tar.lz dir17 0 a/2.txt;
     ./task01.sh -t 25 dir17/archive5.tar.lzma dir16 1 --all;
     ./task01.sh -t 25 dir17/archive6.tar.xz dir17 0 1.txt;
     ./task01.sh -t 25 dir17/archive7.tar.Z dir16 0 a/2.txt;
     ./task01.sh -t 25 dir17/archive8.tlz dir17 0 1.txt a/2.txt;
     ./task01.sh -t 25 dir17/archive9.tbz2 dir16 1 a/2.txt;

 26) [x] Pack the directory structure with files while preserving all permissions and
     attributes. Multiple files and directories supported.

     This task allows you to pack multiple files and directories into an archive while
     preserving their permissions and attributes. The supported archive formats include:
     - *.tar.gz, *.tgz, *.tar.Z
     - *.tar.bz2, *.tbz2, *.tlz
     - *.tar.lzma, *.tlzma, *.tar.xz, *.txz
     - *.tar, *.tar.lz

     IMPORTANT: To use Task 26 options, you need to install 'compress' or 'ncompress'
     for '.Z' archives, 'Lzip' for '.lz' and '.lzma' archives and 'bzip2' executable,
     which is used to handle '.bz2'.

     You can provide the path to the archive as the first argument, followed by the
     directories and files you want to include in the archive. If the specified archive
     directory does not exist, the script will create it.

     Example:
     ./task01.sh -t 26 path/to/archive dir1 file1 .. dir(n) file(n)

     IMPORTANT: When using this task as part of a chain of tasks, it should be executed
     at the end to ensure that all necessary files and directories are included in the
     archive. Please refer to the 'Chaining' section for more details on how to utilize
     this task in a script chain.

     Examples:
     ./task01.sh -t 26 dir20/archive10.tgz dir dir2 dir3;
     ./task01.sh -t 26 ~/dir20/archive11.tlzma dir5 dir4/hi.txt;
     ./task01.sh -t 26 dir20/archive12.txz dir3/hello.txt dir5 dir2;
     ./task01.sh -t 26 dir20/archive1.tar dir3/script2.py dir2 dir5;
     ./task01.sh -t 26 dir20/archive2.tar.gz dir/test.sh dir/file2 dir4;
     ./task01.sh -t 26 dir20/archive3.tar.bz2 dir18/sym.sh dir18/dir2/.htaccess dir;
     ./task01.sh -t 26 dir20/archive4.tar.lz dir19/files dir5;
     ./task01.sh -t 26 dir20/archive5.tar.lzma dir19/files/file1.txt dir16;
     ./task01.sh -t 26 dir20/archive6.tar.xz dir19/files/file2.txt dir;
     ./task01.sh -t 26 dir20/archive7.tar.Z dir19/files/file3.txt dir2;
     ./task01.sh -t 26 dir20/archive8.tlz dir19 dir dir5;
     ./task01.sh -t 26 dir20/archive9.tbz2 dir19 dir18;

 27) Recursively copy the directory structure from the specified directory
     (excluding files).

     This task involves recursively copying the directory structure (excluding files)
     from a specified source directory to a destination directory. The script ensures
     that the destination directory is created if it does not already exist.

     The script utilizes the find command with the '-type d' option to identify all
     directories within the source directory. For each directory found, the 'mkdir -vp'
     command is used to create the corresponding directory structure in the destination
     directory. The -p option ensures that the command creates parent directories as
     needed, and it avoids any errors if the destination directory already exists.

     By executing this task, the directory hierarchy from the source directory will be
     replicated in the destination directory without copying any files, making it ideal
     for duplicating the structure of complex directory trees.

     Example: ./task01.sh -t 27 dir6 dir20;

 28) Display a list of all system users (names only) in alphabetical order.

     This task aims to provide a list of all system users, displaying only their
     usernames, in alphabetical order. The list is retrieved from the 'etc/passwd' file,
     which contains information about all users on the system.

     By using the 'cut' command to extract the usernames and the 'sort' command to
     arrange them alphabetically, the output presents a concise list of all system
     users' names for easy reference.

     Example: ./task01.sh -t 28;

 29) Display a list of all system users sorted by ID, in the format: login id.

     This task aims to provide a sorted list of all system users along with their
     corresponding user IDs (UIDs). The list is retrieved from the '/etc/passwd' file,
     which contains information about user accounts on the system.

     The 'cut' command is used to extract the login names and UIDs from the file,
     and then the 'sort' command is used to arrange the users based on their UIDs
     in ascending numerical order. The final output displays each user's login name
     followed by their corresponding UID, making it easier to identify and manage
     system users.

     Example: ./task01.sh -t 29;

 30) Display a list of all system users (names only) sorted by ID in reverse order.

     Task 30 aims to display a list of all system users (names only) sorted by their
     User IDs (UIDs) in reverse order. The command extracts the user names and UIDs from
     the '/etc/passwd' file, sorts the list based on the UIDs in reverse numeric order,
     and finally displays the sorted list of user names.

     This allows you to see the system users with the highest UIDs first and the lowest
     UIDs last.

     Example: ./task01.sh -t 30;

 31) Display all users who do not have the right to log in or do not have a login shell
     in the system. (two commands)

     This task involves using two commands to display two lists of users:

     a) Users without login rights:
        This command searches the '/etc/shadow' file and identifies users who do not
        have the right to log in.

     b) Users without a login shell:
        This command searches the '/etc/passwd' file and identifies users who do not
        have a login shell specified.

     The lists will provide information about users who are restricted from logging into
     the system or do not have a valid login shell defined.

     NOTE: The information from these commands is important for managing user access and
     system security.

     Example: ./task01.sh -t 31;

 32) List all users who have or do not have a terminal (bash, sh, zsh, etc.).
     (Two commands).

     This task involves two commands to list users based on whether they have a terminal
     shell (such as bash, sh, zsh, etc.) or do not have a terminal shell (using
     '/bin/false' or '/sbin/nologin' shells).

     The first command lists users with a terminal, while the second command lists users
     without a terminal. The information is extracted from the '/etc/passwd' file.

     Example: ./task01.sh -t 32;

 33) Download all links to href resources on a web page.
     a) Use curl and wget. Download in parallel.
     b) Provide recommendations for usage.

     This task allows you to download all links to href resources found on a specified
     web page. The script uses curl and wget to download the resources in parallel,
     making the process more efficient.

     Before proceeding, it checks the URL to ensure it returns an HTTP status code of
     200 using 'curl' and '0'. Additionally, it creates the output directory if it does
     not exist.

     Explanation of Commands Used:

     'curl -s': Retrieves the content of the specified URL in silent mode (-s).
     'grep -oE 'href="[^"]+"'': Extracts all occurrences of href attributes with their
     corresponding URLs from the HTML content.
     'cut -d'"' -f2': Extracts only the URLs from the href attributes using the
     double-quote delimiter (").
     'xargs -P 10 -I {}': Runs wget in parallel with up to 10 instances using {} as the
     placeholder for each URL.
     'wget -nv -P "" "{}"': Downloads the resources with the URL provided by
     {}, displaying no output (-q) and saving them in the specified output directory
     ('-P ""').

     Before downloading, the script performs the following checks:

     Verifies the URL's status code using curl and '0' to ensure it returns a 200
     status code. Creates the output directory specified by the user if it does not
     exist.

     This task allows you to efficiently download resources from a web page, making it
     easier to access all the necessary href links in one go.

     NOTE: Remember to use this feature responsibly and avoid overloading the web server
     with too many simultaneous requests.

     Example: ./task01.sh -t 33 www.columbia.edu/~fdc/sample.html dir21;

 34) Terminate processes that have been running for more than 5 days.

     This task allows you to terminate processes that have been running for more than 5
     days. It provides two options for terminating such processes: 'a' and 'b'.

     Option 'a' (killall):
     This option uses the killall command to find and terminate non-critical processes
     that have been running for more than 5 days, excluding specific system processes
     like systemd, sshd, and kworker.

     Option 'b' (no ps / killall):
     This option achieves the same task without relying on the ps and killall commands.
     It iterates through the /proc directory to find processes that meet the 5-day
     criteria and then terminates them using the kill -9 command. Similarly, it excludes
     specific system processes.

     IMPORTANT: Using these options may result in the termination of processes that have
     been running for more than 5 days, except for specific system processes. Use with
     caution.

     Examples:
     ./task01.sh -t 34 a
     ./task01.sh -t 34 b

 35) Delete files with a certain extension if there is no corresponding file with a
     different extension in the same directory ( e.g. *.txt & *.jpeg).

     This task allows you to delete files of a specified extension if there is no
     corresponding file with a different extension in the same directory. The script
     ensures that you provide three arguments: the target directory, the file type to
     delete, and the file type to compare.

     The script handles user input for file types to delete and compare. You can specify
     file types either with or without a leading dot (e.g., .txt or txt). The script will
     automatically handle the input appropriately by adding a wildcard * before the
     extension.

     IMPORTANT: You should avoid using wildcards (e.g., *) when specifying file types as
     arguments, especially when calling the script from the command line. If you need to
     use wildcards, make sure to quote the file type arguments to prevent shell expansion.
     For example:
     Correct: ./task01.sh -t 35 path/to/dir1 jpeg txt; or
              ./task01.sh -t 35 path/to/dir1 .jpeg .txt;

     Incorrect: ./task01.sh path/to/dir1 *.jpeg *.txt;

     Before performing the deletion, the script checks if there are any files matching
     the provided file types in the directory. If no files are found, it will display
     appropriate messages and exit.

     IMPORTANT: If the script does not find corresponding files with another extension
     in the subdirectories (however, they can be found in the parent directory), it will
     still delete all files with the specified type as the second argument from the
     subdirectories.

     The script uses a function called 'file_type' for file type handling, which
     automatically adds the necessary wildcard based on the input. It then employs the
     find command to search for files with the specified file type to delete.

     For each file found, the script checks if a corresponding file with the compare file
     type exists in the same directory. If not, it will delete the file.

     After the deletion process, the script displays the directory information before and
     after the operation, along with a message indicating which files were deleted.

     Examples:
     ./task01.sh -t dir30 .jpeg txt;
     ./task01.sh -t dir31 txt .jpeg;

 36) Find your IP address using the command line.

     This task allows you to find either your private or public IP address through the command line. Two options are allowed:
        - 'pr': Find your private IP address using the 'hostname' command.
        - 'pb': Find your public IP address using the 'curl' command and the
        'checkip.amazonaws.com' service.

     The script performs the appropriate command and displays the resulting IP address
     based on your chosen option.

     You can select either option as an argument while running the script.

     Examples:
     ./task01.sh -t 36 pr      (Finds your private IP address)
     ./task01.sh -t 36 pb      (Finds your public IP address)

 37) Get all IP addresses from a text file.

     In Task 37, the objective is to extract and display all valid IPv4 addresses
     present in a given file. The task operates in silent mode by default, providing
     a concise output that shows only the valid IPv4 addresses found in the file.

     By default, when you execute the script with Task 37 and specify the file, it will
     display the contents of the file followed by the valid IPv4 addresses found in the
     file. If no valid IPv4 addresses are found, it will indicate that there are no
     valid IP addresses in the file.

     NOTE: You can also choose the 'verbosity mode' ('-v') as an additional option. When
     you specify '-v' along with the file, it will provide a more detailed output,
     validating each line in the file to check whether it contains a valid IPv4 address.

     For each valid address, it will indicate whether it is a private or public IPv4
     address.

     To summarize, you can use Task 37 with the following options:
     - Without '-v': Silent mode (default), showing only valid IPv4 addresses in the
     file.
     - With '-v': Verbosity mode, validating and displaying the classification of each
     IP address (private or public).

     Examples:
     ./task01.sh -t 37         (Default mode, IPs only)
     ./task01.sh -t 37 -v      (Verbosity mode, IPs with info)

 38) [x] Check hosts from the command line input or file by ping or nmap utility.

     Task 38 provides a versatile and user-friendly solution to check the activity of
     multiple IP addresses and networks using the 'ping' or 'nmap' utility. It supports
     various features, including validating IPs and networks, specifying IP ranges using
     brace expansion, and the ability to check active hosts only.

     NOTE: If you choose to use 'nmap' but it's not found on the system, the script
     should prompt you to install it. This ensures that the necessary utility ('nmap') is
     available for scanning the hosts effectively.

     Features:
     - Supported Networks and Prefixes: The script supports all three major private
       networks (10.0.0.0/8, 172.16.0.0/16, and 192.168.0.0/16) and their corresponding
       prefixes (e.g., /24) for checking active hosts.

     - IP and Network Validation: The script validates the provided IP addresses and
       networks to ensure only valid entries are processed.

     - Ping and Nmap: The script allows users to choose between using 'ping' or 'nmap'
       to check host activity.

     - Active and Inactive Hosts: By default, the script provides information about both
       active and inactive hosts. It can also show active hosts only using the "-a" flag.

     - Warning for Large Scans: The script issues a warning if a large list of IPs or
       networks is being scanned, providing users the option to proceed or not.

     - Handling No Active Hosts: If no active hosts are found during the scan, the script
       notifies the user.

     - Error Handling for No Valid IPs: The script alerts the user if no valid IP addresses 
       or networks are provided.

     - Pipe and Chaining Support: The script can be used with scripting pipes and
       chaining, allowing for flexible use with other commands.

     Examples:
     - Ping Multiple IPs/Networks:
       ./task01.sh -t 38 192.168.0/24 10.255.255.255 1.1.1.1 192.168.0.{1..4}
       ./task01.sh -t 38 255.255.255.255 172.16.0.0 221.1.1.2 1.1.1.1 -a

     - No Valid IPs Passed:
       ./task01.sh -t 38 no valid IP addresses or networks
       ./task01.sh -t 38 no valid from a file $(cat dir4/hi.txt) -a

     - Nmap Multiple IPs/Networks:
       ./task01.sh -t 38 192.168.0.0/24 10.255.255.255 221.1.1.2 1.1.1.1 nmap

     - No Active Hosts Found by Nmap:
       ./task01.sh -t 38 255.255.255.255 172.16.255.0 221.1.1.2 221.1.1.87 nmap -a

     - Checking Hosts and Networks by Ping from a File:
       ./task01.sh -t 38 $(cat dir4/hosts-server.txt)

     - Checking Active Hosts and Networks by Ping from a File:
       ./task01.sh -t 38 $(cat dir4/hosts-server.txt) -a

     - Scanning Local Network by Nmap:
       ./task01.sh -t 38 $(ip addr | grep 'inet ' |
       cut -d ' ' -f 6 | head -2 | tail -1) -nmap

     The script empowers users to efficiently manage and verify the activity of multiple
     hosts and networks, making it a valuable tool for network administrators and
     security professionals.

     Additionally, the comprehensive error handling and interactive warnings ensure a
     smooth and reliable experience for the user. For further information and
     advanced usage, please refer to the script's Scripting & Pipes manual chapters.

     IMPORTANT: Ensure that you run this task at the end of the chain when using the
     script in a chain of tasks. Refer to the 'Chaining' section for more details.

     Examples:
     ./task01.sh -t 38 172.16.255.0/24 192.168.0.1 8.8.8.8
     ./task01.sh -t 38 172.16.255.10/24 192.168.0.2 1.1.1.2 -a
     ./task01.sh -t 38 10.16.255.0/8 192.168.0.100 8.8.8.8 nmap
     ./task01.sh -t 38 172.16.255.10/16 1.1.1.2 $(cat dir4/hosts*) nmap -a

 39) Filter the output of Task 36 to show uplink interfaces with assigned IPs.

     Task 39 filters the output of Task 36 to display only the uplink interfaces with
     assigned IP addresses. It uses regular expressions to extract the IP addresses from
     the output of Task 36. Then, it checks if any private IP addresses are assigned.

     If no private IP addresses are found, it displays a message indicating that no
     private IP address is assigned. Otherwise, it looks up the interfaces corresponding
     to the private IP addresses and prints the interface names along with their
     respective IP addresses.

     Example:
     ./task01.sh -t 39

 40) Get all subdomains from an SSL certificate.

     Task 40 is a Bash function that extracts and displays subdomains from an SSL
     certificate using OpenSSL. If no subdomains are found, it will show a message
     indicating the absence of subdomains in the certificate.

     Example:
     ./task01.sh -t 40 dir50/example.crt

 41) Extract the file path, name, and extension from a given file path.

     This task involves creating a Bash function that takes a file path string as an
     argument. The function then extracts the directory path, file name (without
     extension), and file extension from the given string and displays them as output.

     If the file has no extension, it will indicate this in the output. The function
     uses 'dirname', 'basename', and parameter expansion to perform the extraction.

     Example:
     ./task01.sh -t 41 dir4/files/file2.txt

 42) Delete files in a specified directory and its subdirectories based on their length
     and type.

     This task enables you to delete files within a specified directory and its
     subdirectories based on two criteria: their file type and size. You need to provide
     three arguments for this task:

     - Directory: The path to the directory in which the deletion will be performed.

     - Type: The file type you want to delete. You can use wildcards (*, ?) to match
     multiple file names, but ensure that you quote them to avoid unexpected script
     behavior.

     - Size: The file size threshold for deletion. Files larger than the specified size
     will be deleted.

     IMPORTANT: The script supports wildcards in the type argument. However, it is
     essential to enclose them in double quotes (e.g. ".txt" or '.txt') to prevent the
     shell from expanding them prematurely and potentially causing unintended file
     deletions.

     The script will validate the provided size to ensure it follows a specific format.
     Once the size is validated, it will use the 'find' command to identify and delete
     files that match the specified type and size criteria.

     If files are deleted, their names will be displayed as output. If no files meet the
     criteria for deletion, the script will display the message:
     "No files deleted. Check file size."

     CAUTION: Ensure you understand the consequences of deleting files before running
     this task. Verify the provided arguments to avoid accidental data loss or removal
     of essential files.

     Example:
     ./task01.sh -t 42 dir55 "*.txt" 8192c

 43) Create files in a designated directory using a file that contains file names and
     their corresponding hash IDs, and add the hash ID entry to each file.

     This task involves reading a file that contains a list of file names and their
     associated hash IDs (4-byte identifiers). For each entry in the file, a new file is
     created in the specified directory, and the corresponding hash ID is added to the
     file. This process ensures that each file is uniquely identified by its hash value.

     The task also provides visual feedback by displaying the results, making the
     process more interactive and informative.

     Example:
     ./task01.sh -t 43 dir55/list.txt dir77

 -d, --demo    Demo Mode. See above.

 -h, --help    Display this help
</pre>

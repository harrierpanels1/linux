#!/usr/bin/env bash
#set -x
### Global vars & functions ########################################################
script_name=$(basename -- "$0")

# Help & Manual
man() {
    cat <<- EOF

Usage: ./$script_name [OPTIONS]
Examples: ./$script_name -t 1 file; ./$script_name --task 6 dir1 dir2 cksum;

$(message_with_animated_or_static_bar "Script features:" 17 - 0)
The script performs up to 45 Bash & Linux related tasks without using 'sed' or 'awk'.
It has the following features: Demo Mode, Scripting, Pipes, and Chaining.

$(message_with_animated_or_static_bar "[d] Demo Mode:" 17 - 0)
This feature is implemented for testing purposes. It utilizes the SandBox functions to
create a temporary environment that contains the necessary files to test each task's
performance. The results of each task will be displayed in the console.

To start Demo Mode, run the following command:
./$script_name -t -d /path/to/directory or ./$script_name -t --demo /path/to/directory

The SandBox functions will first download the necessary archives and extract them to
the folder you specify. Then, each task will be run one by one, starting from the first,
and the output for each task will be displayed until Demo Mode reaches the final task.
After that, all temporary content will be removed.

IMPORTANT: Make sure you have an active internet connection so that the SandBox functions
can download the necessary archives. Specify a directory for Demo Mode that does not
already exist. The SandBox functions will create it for you.

$(message_with_animated_or_static_bar "Scripting:" 17 - 0)
The script is highly flexible and supports various scenarios, including scripting.
If you need to pass a list of options such as directories or files as arguments on the
command line, there are multiple ways to do it!

For example, in Task 18, you can pass files as arguments with the last argument being
a directory. One way is to use the 'echo' command to pass the arguments in a variable:

./$script_name -t 18 \$(echo "path/to/file1 path/to/dir4/file2 path/to/dir5")

This will pass three arguments to the script: a list of files (file1 and file2) and
a directory (dir5) with a relative path. Here's another example using scripting in the
command line:

./$script_name -t 18 \$(for file in \$(ls -h path/to/dir4/dir5); \\
do \\
    echo "path/to/dir4/dir5/\$file"; \\
done) path/to/dir4

This will pass all files with a relative path from the directory path/to/dir4/dir5
as arguments to the script. Similarly, you can use a 'while' loop with process
substitution:

args="" && while read file; do \\
    args="\$args path/to/dir/with/files/\$file"; \\
done < <(ls -h path/to/dir/with/files) &&
./$script_name -t 18 \$args path/to/dir

This will assign a list of files to the 'args' variable and pass them as arguments to
the script. Another approach is to use a 'for' loop:

args="" &&
for file in path/to/dir/with/files/*; do \\
    args="\$args \$file"; \\
done &&
./$script_name -t 18 \$args path/to/dir

In this case, the for loop iterates over the files in the specified directory, and
the 'args' variable is constructed with the file paths. Finally, the 'args' variable is
passed as arguments to the script.

If you have a file with a list of files, you can use the 'cat' command to pass them as
arguments:

./$script_name -t 18 \$(cat path/to/file) path/to/dir

This will pass a list of files as arguments to the script, reading them from the
specified file. Alternatively, you can use a 'while' loop with input redirection:

while read file; do \\
    ./$script_name -t 18 \$file path/to/dir; \\
done < path/to/file

This will pass a list of files as arguments to the script, reading them from the file
specified in the input redirection.

$(message_with_animated_or_static_bar "Pipes:" 17 - 0)
Using pipes adds even more flexibility and functionality to the script. Let's explore
the same scenarios mentioned in the "Scripting" section. For example, if we need to pass
files to the script as required in Task 18, we can use the following approach:

args=\$(ls -h path/to/dir/with/files |
    while read -r file; do \\
        echo "path/to/dir/with/files/\$file"; \\
    done) &&
./$script_name -t 18 \$args path/to/dir

In this example, the 'args' variable is passed with a list of files as arguments to
the script. If you have a file with a list of files, and the last entry in the list is
a directory, you can use the 'cat' command in a pipe:

cat path/to/file | xargs ./$script_name -t 18

This will pass a list of files as arguments to the script, reading them from the file
specified with 'cat'. You can also use 'xargs' and the 'cat' command to process the file:

cat path/to/file | xargs -I {} ./$script_name -t 18 {} path/to/dir

This approach uses 'xargs' with the -I option to specify a placeholder {} for each
argument. Another option is to use 'xargs' with the output of the 'ls' command for a
directory with files:

ls -1 path/to/dir/with/files |
xargs -I {} ./$script_name -t 18 path/to/dir/with/files/{} path/to/dir

This will pass the file paths as arguments to the script. Lastly, you can achieve the
same result using the 'cat' command and a 'for' loop in a variable:

ls -1 path/to/dir/with/files |
./$script_name -t 18 \$(for file in \$(cat); do \\
    echo path/to/dir/with/files/\$file; \\
done) path/to/dir

This approach uses a 'for' loop to construct the file paths, which are then passed as
arguments to the script. If you want to pass arguments from a file using the 'cat'
command:

cat path/to/file | ./$script_name -t 18 \$(cat) path/to/dir

Feel free to experiment with different options and combinations, including scripting,
piping, and chaining, to explore different subsets of code based on your requirements.

$(message_with_animated_or_static_bar "Chaining:" 17 - 0)
The script supports task chaining. That means you can specify as many tasks in the
command line as you like with their options accordingly, and they will be executed
in the order you specify.

Ensure that you provide the exact number of arguments required by each task when
chaining commands together. Refer to the examples provided for guidance.

IMPORTANT: Tasks that are marked with [x] should be run at the end of the chain! One
task marked with [x] per chain only! If you need to run more than one task marked with
[x] use && to link another instance of the script with the task marked with [x].

Examples:
./$script_name -t 2 dir ec2-user ec2-user -t 3 dir -t 1 file -t 6 dir1 dir2 cksum;
./$script_name -t 15 -t 6 dir2 cksum && ./$script_name -t 18 dir/file1 dir/file2 dir5;

  Options:

 -t, --task TASK_NUMBER    A mandatory option followed by a task number

  1) List all system groups and retrieve their unique names and IDs. Save the output
     to a file.

     This task involves retrieving a list of all system groups on the Linux system and
     extracting their unique names and corresponding IDs. The output of the command will
     be saved to a file for further reference or analysis.

     NOTE: If the specified file directory does not exist, the script will create it.

     Example: ./$script_name -t 1 file;

  2) Find all files and directories that have corresponding user and group permissions
     in a specified directory.

     This task involves searching for files and directories within a specified directory
     that have matching user and group permissions. It helps identify the files and
     directories where the user and group ownership aligns with the specified permissions.

     Example: ./$script_name -t 2 dir ec2-user ec2-user;

  3) Find all scripts in a specified directory and its subdirectories.

     This task aims to locate all script files within a specific directory and its
     subdirectories. It searches for files with specific file extensions (e.g., .sh, .py,
     .rb) that are commonly associated with script files. By traversing the directory
     tree, it identifies and lists all script files found, including their paths.

     This enables easy access and management of script files within the specified
     directory and its subdirectories.

     NOTE: The specific file extensions for scripts can vary based on the programming or
     scripting languages used.

     Example: ./$script_name -t 3 dir;

  4) Search for script files by a specific user in a specified directory.

     This task searches for script files within a specified directory that are owned by
     a specific user. It helps to identify and locate script files that belong to a
     particular user within the given directory.

     Example: ./$script_name -t 4 /home/ec2-user/dir ec2-user;

  5) Perform a recursive (case-sensitive) word or phrase search for a specific file type
     in a specified directory.

     This task involves conducting a recursive search for a specific file type within a
     specified directory. It includes the additional requirement of performing a
     case-sensitive search for a particular word or phrase within the files of that file
     type.

     The search aims to identify and locate files that contain the desired word or phrase,
     enabling targeted analysis or investigation within the specified directory.

     IMPORTANT: To ensure proper functioning of the script, it is important to specify
     the file extension correctly. The script has a logic to handle the file extension,
     so it is recommended to use a dot followed by the file extension (e.g. .txt) or the
     extension without the dot (e.g. txt).

     NOTE: If you need to use wildcards (e.g. *txt or *.txt), it is recommended to
     enclose them in single or double quotes (e.g. '*txt' or '*.txt'). This will ensure
     that the script interprets the wildcard correctly and prevents unexpected behavior.

     Example: ./$script_name -t 5 dir sh bash;

  6) [x] Find duplicate files in specified directories. First compare by size, then by
     option (choose a hash function: CRC32 command: cksum; MD5 command: md5sum; SHA-1
     command: sha1sum; SHA-224 command: sha224sum). The result should be sorted by
     filename.

     NOTE: In this task, you can specify multiple directories by using the following
     format: -t dir1 dir2 ... dir(n) hash_function_command. You can include as many
     directories as needed, followed by the hash function command.

     IMPORTANT: The task should be run at the end of the chain! See more in the
     'Chaining' section.

     Example: ./$script_name -t 6 dir1 dir2 cksum;

  7) Find all symbolic links to a file by its name and path.

     IMPORTANT: Remember that when using the option you should always use the path
     to the target file as it appears in the symbolic link, whether it is an absolute
     or relative path.

     TIP: If you don't know what path is specified in the symbolic link use an absolute
     or relative path to the file and the script will try to handle it in the best way
     possible.

     Examples:
     ./$script_name -t 7 dir2 $HOME/dir/file;
     ./$script_name -t 7 dir2 dir/file;

  8) Find all hard links to a file by its name and path.

     This task involves searching for all hard links to a specific file based on its name
     and path. Hard links are multiple entries in the file system that point to the same
     underlying file data. By using the name and path of the file as search criteria, you
     can identify all other hard links that share the same file data.

     Example: ./$script_name -t 8 dir2 dir/file;

  9) Given only the inode of a file, find all its names.

     This task involves finding all the names associated with a file based on its inode.
     The inode is a data structure in a file system that stores metadata about a file,
     including its unique identifier.

     By using the inode number, this task aims to identify all the names by which the
     file is referenced within the file system.

     Example: ./$script_name -t 9 33721754;

 10) Given only the inode of a file, find all its names taking into account that there
     may be multiple mounted sections.

     This task extends the previous one by considering the possibility of multiple
     mounted sections. A mounted section refers to a separate file system that is mounted
     at a specific location in the directory tree.

     When a file is present in multiple mounted sections, it can have different names
     associated with its inode in each section. Therefore, this task aims to identify all
     the names associated with the given inode, considering the presence of multiple
     mounted sections.

     Example: ./$script_name -t 10 33721754;

 11) Correctly delete a file, taking into account the possibility of the existence
     of symbolic or hard file links.

     This task involves deleting a file while considering the presence of symbolic or
     hard file links. Deleting a file with links requires careful handling to ensure that
     all associated links are correctly updated or removed to maintain data integrity.

     By accounting for both symbolic and hard file links, the deletion process ensures
     that all related references to the file are appropriately handled to prevent any
     issues with link consistency or orphaned links.

     IMPORTANT: Use the task with caution as it may delete symbolic links that point to
     another file with the same name as the specified file. Please ensure that you only
     use this task when you are certain that you want to delete all files and links that
     match the specified file.

     Example: ./$script_name -t 11 dir/delete.txt;

 12) Recursively change access rights to files (specified by file mask) in a specified
     directory.

     This task involves modifying the access rights (permissions) of files within a
     specified directory. It applies to files that match a specified file mask or pattern.

     The access rights can include permissions such as read, write, and execute for the
     owner, group, and others. By recursively changing the access rights, it affects all
     files within the directory and its subdirectories that match the specified file mask.

     NOTE: The specific access rights to be modified and the file mask pattern should be
     provided as arguments when executing the task.

     IMPORTANT: Make sure you specify a file mask in quotes! Refer to the examples
     below for guidance.

     Examples:
     ./$script_name -t 12 dir3 '*.txt' 644;
     ./$script_name -t 12 dir3 "*t" u+x,o+w;

 13) Compare two directories recursively and display only the files that differ. For
     each file that differs, print two lines of output, and starting from the third line,
     print the lines that differ between the two files.

     This task involves comparing the contents of two directories, including all
     subdirectories, and identifying the files that have differences between them.
     The output will show only the files that differ, allowing for easy identification of
     the differing files in the compared directories.

     Example: ./$script_name -t 13 dir3 dir4;

 14) Get MAC addresses of network interfaces.

     This task involves retrieving the MAC addresses (Media Access Control addresses) of
     the network interfaces on a system. MAC addresses are unique identifiers assigned to
     network devices, and obtaining them can be useful for various network-related
     operations and configurations.

     Example: ./$script_name -t 14;

 15) Display the list of users currently authorized in the system.

     This task aims to provide a brief description of displaying the list of users who
     are currently authorized or logged in to the system. It helps in obtaining an
     overview of the active user sessions or logged-in users on the system.

     Example: ./$script_name -t 15;

 16) Display a list of active network connections in the form of a table:
     connection type and the number of connections.

     This task aims to provide a clear and organized view of the current active network
     connections on the system. The output will be formatted as a table, displaying
     relevant information about each network connection. This includes details such as
     the connection type, status, and associated endpoints (source and destination).

     By presenting the network connections in a table format, it becomes easier to
     analyze and understand the active network connections at a glance. This can be
     useful for monitoring network activity, identifying established connections, and
     troubleshooting network-related issues.

     Example: ./$script_name -t 16;

 17) Re-assign an existing symbolic link.

     This task involves changing the target of an existing symbolic link to point to
     a different file or directory. It allows you to update the reference of the symbolic
     link to a new location while preserving the link itself. The original link remains
     intact, but its destination is modified to the specified target.

     Note that if the existing link points to a file or directory that has been
     deleted or moved, the link will be broken and the script will create a new
     link that points to the new target.

     Example: ./$script_name -t 17 script.py dir4/script_soft.py;

 18) [x] Create symbolic links to multiple files specified by their relative paths,
     using a directory path as the location for the symbolic links.

     NOTE: In this task, you can specify multiple files using the following format:
     -t file1 file2 ... file(n) dir. You can include as many files as needed, followed
     by a path to the directory in which the symbolic links to the files will be created.

     If you specify an absolute path to a file (e.g. /home/user/path/to/file), don't
     worry! The script will still create a relative symbolic link. You can also specify
     either relative or absolute path to a directory.

     NOTE: If the specified directory does not exist, the script will create it. If you
     specify a system path for a destination directory (e.g., /tmp/dir),the script will
     create relative symbolic links for all files within the directory. Similarly, if
     you specify a system path for a file, the script will create a relative symbolic
     link to the file.

     NOTE: If you specify a directory that is the root directory of the file you want to
     create a symbolic link for, no link will be created for the file. This is because
     the script creates symbolic links with the same name as the original file specified.
     However, you will see a message indicating that a link was created, even though no
     link was actually created.

     If you have a file with a list of files, you can use the 'cat' command to pass them
     as arguments to the script on the command line:

     ./$script_name -t 18 \$(cat path/to/file) path/to/dir

     Refer to the 'Scripting' and 'Pipes' section of the manual for more information.

     IMPORTANT: The task should be run at the end of the chain! See more in the
     'Chaining' section.

     Examples:
     ./$script_name -t 18 $HOME/dir4/hi.txt dir3/script2.py dir5/d/e;
     ./$script_name -t 18 dir4/hi.txt dir3/script2.py /tmp;
     ./$script_name -t 18 dir4/hi.txt /tmp/file.tmp $HOME/dir5;

 19) Copy a directory containing hard links, direct and relative symbolic links,
     preserving attributes and permissions, for backup on removable storage. Provide
     two versions using 'cp' and 'rsync' commands.

     This option allows you to copy a directory or its contents while preserving soft
     and hard links, ownership, permissions, and attributes for backup purposes. Two
     versions are provided using the 'cp' and 'rsync' commands.

     To use this option, specify the command you want to use ('cp' or 'rsync') followed
     by the source directory and the destination directory.

     If the destination directory does not exist, the script will create it for you.
     If you want to copy the directory's contents only, specify it as 'dir/.' with a
     relative or absolute path to the directory.

     IMPORTANT: Do not use wildcards like 'dir/*' if you want to copy the contents only.
     Instead, use 'dir/.'

     Examples:
     ./$script_name -t 19 cp /tmp/dir dir5;
     ./$script_name -t 19 rsync dir4/. /tmp;

 20) Copy the directory, taking into account that it contains hard and soft links to
     files, both direct and relative.

     This option is similar to Task 19 but less strict, preserving only the hard and
     soft links. It creates the destination directory if it doesn't exist. You can use
     either 'cp' or 'rsync' to copy the entire directory or just its contents if
     specified as 'dir/.'.

     IMPORTANT: Do not use wildcards like 'dir/*' if you want to copy only the contents.
     Instead, use 'dir/.'

     Examples:
     ./$script_name -t 20 cp dir dir5;
     ./$script_name -t 20 rsync dir4/. ~/dir5/dir;

 21) Copy all files and directories from the specified directory to a new location
     while preserving attributes and permissions.

     This option is similar to Tasks 19-20 with only requirement being to preserve
     permissions and attributes. It creates the destination directory if it doesn't
     exist. You can use either 'cp' or 'rsync' to copy the entire directory or just
     its contents if specified as 'dir/.'.

     IMPORTANT: Do not use wildcards like 'dir/*' if you want to copy only the contents.
     Instead, use 'dir/.'

     Examples:
     ./$script_name -t 21 cp dir/. dir5;
     ./$script_name -t 21 rsync ~/dir4 dir5/dir;

 22) In the project directory, convert all relative links to direct links.

     This task aims to convert all relative links within the project directory to direct
     links. By doing so, the links will point directly to the absolute path of their
     targets.

     This conversion ensures that the links remain valid and functional, even if the
     project directory or the referenced files are moved or accessed from different
     locations.

     NOTE: It's essential to exercise caution when performing this conversion, as it
     alters the behavior of the links within the project directory.

     Example:
     ./$script_name -t 22 dir14;

 23) In the project directory, convert all direct links to relative links to the project
     directory.

     This task aims to convert all direct symbolic links within the project directory to
     relative links referencing the project directory. By doing so, the links will point
     to the target file or directory using a relative path from the project directory.

     This conversion ensures that the links remain valid and maintain their functionality
     even when the project directory is moved or accessed from different locations. It
     allows for easier portability and flexibility when working with symbolic links
     within the project.

     NOTE: If the conversion encounters any issues with the 'realpath' command, please
     ensure that the required files and directories exist and that the script has
     appropriate permissions to access them.

     Example:
     ./$script_name -t 23 dir14;

 24) Find all broken links in the specified directory and delete them.

     This task aims to identify and remove broken symbolic links within the specified
     directory and its subdirectories. Broken links are symbolic links that point to
     non-existent targets.

     The command uses the 'find' utility to search for broken links using the '-xtype l'
     option. Once found, the links are deleted using the '-delete' option. Additionally,
     the command searches for subdirectories ('-type d') and recursively deletes broken
     links within each subdirectory.

     Note: Exercise caution when using this command as it permanently deletes broken
     symbolic links.

     Example:
     ./$script_name -t 24 dir15;

 25) [x] Unpack all contents or multiple directories or files from an archive to a
     specified location.

     This task allows you to unpack a specific directory or file from different archive
     formats to the destination directory. The supported archive formats include:
     - *.tar.gz, *.tgz, *.tar.Z
     - *.tar.bz2, *.tbz2, *.tlz
     - *.tar.lzma, *.tlzma, *.tar.xz, *.txz
     - *.tar, *.tar.lz

     The files will be unpacked using the 'tar' command. Please make sure you have the
     necessary compression utilities installed for the corresponding archive formats.

     IMPORTANT: To use Task 25 options, you need to install 'compress' or 'ncompress'
     for '.Z' archives, 'Lzip' for '.lz' and '.lzma' archives and 'bzip2' executable,
     which is used to handle '.bz2'.

     Please ensure that you provide the arguments in the correct order to extract the
     content accurately. For each archive format, consider the compression options
     applicable to extract the content correctly.

     To unpack the desired content, you need to specify the arguments in the following
     order:
     ./$script_name -t 25 path/to/archive path/to/dir strip-option file1 .. file(n)

     where 'path/to/dir' is the destination directory, strip-option - the number of
     parent directories to skip when extracting (see more below), file or files and/or
     directories to extract from the archive. The script supports extracting multiple
     files or directories within the archive or just a single one.

     NOTE: If the destination directory does not exist, the script will create it for you.

     The strip-option:
     The strip-option determines how many leading components of the directory structure
     should be stripped during extraction. For example, specifying '--strip-components=1'
     will remove the top-level directory from the extracted contents.

     This option is useful when you want to extract only specific subdirectories or files
     without including the parent directory structure.

     NOTE: If the specified strip-option value exceeds the number of leading components
     in the archive, no files will be extracted.

     IMPORTANT: If you want to keep the parent directory structure as is you should
     specify the strip-option as '0'. For example:
     ./$script_name -t 25 path/to/archive path/to/dir 0 dir1 file1 .. dir(n) file(n)

     '--all' feature:
     The '--all' feature allows you to extract all contents from the archive. For example:
     ./$script_name -t 25 path/to/archive path/to/dir 1 --all

     In this example, the strip-option is set to 1, which means it will skip one parent
     directory when extracting all contents.

     IMPORTANT: Ensure that you run this task at the end of the chain when using the
     script in a chain of tasks. Refer to the 'Chaining' section for more details.

     Examples:
     ./$script_name -t 25 dir17/archive10.tgz dir16 0 --all;
     ./$script_name -t 25 ~/dir17/archive11.tlzma dir17 1 a;
     ./$script_name -t 25 dir17/archive12.txz dir16 1 a/2.txt;
     ./$script_name -t 25 dir17/archive1.tar dir17 0 3.txt 1.txt 4.txt a/2.txt;
     ./$script_name -t 25 dir17/archive2.tar.gz dir16 0 3.txt a;
     ./$script_name -t 25 dir17/archive3.tar.bz2 dir16 0 1.txt;
     ./$script_name -t 25 dir17/archive4.tar.lz dir17 0 a/2.txt;
     ./$script_name -t 25 dir17/archive5.tar.lzma dir16 1 --all;
     ./$script_name -t 25 dir17/archive6.tar.xz dir17 0 1.txt;
     ./$script_name -t 25 dir17/archive7.tar.Z dir16 0 a/2.txt;
     ./$script_name -t 25 dir17/archive8.tlz dir17 0 1.txt a/2.txt;
     ./$script_name -t 25 dir17/archive9.tbz2 dir16 1 a/2.txt;

 26) [x] Pack the directory structure with files while preserving all permissions and
     attributes. Multiple files and directories supported.

     This task allows you to pack multiple files and directories into an archive while
     preserving their permissions and attributes. The supported archive formats include:
     - *.tar.gz, *.tgz, *.tar.Z
     - *.tar.bz2, *.tbz2, *.tlz
     - *.tar.lzma, *.tlzma, *.tar.xz, *.txz
     - *.tar, *.tar.lz

     IMPORTANT: To use Task 26 options, you need to install 'compress' or 'ncompress'
     for '.Z' archives, 'Lzip' for '.lz' and '.lzma' archives and 'bzip2' executable,
     which is used to handle '.bz2'.

     You can provide the path to the archive as the first argument, followed by the
     directories and files you want to include in the archive. If the specified archive
     directory does not exist, the script will create it.

     Example:
     ./$script_name -t 26 path/to/archive dir1 file1 .. dir(n) file(n)

     IMPORTANT: When using this task as part of a chain of tasks, it should be executed
     at the end to ensure that all necessary files and directories are included in the
     archive. Please refer to the 'Chaining' section for more details on how to utilize
     this task in a script chain.

     Examples:
     ./$script_name -t 26 dir20/archive10.tgz dir dir2 dir3;
     ./$script_name -t 26 ~/dir20/archive11.tlzma dir5 dir4/hi.txt;
     ./$script_name -t 26 dir20/archive12.txz dir3/hello.txt dir5 dir2;
     ./$script_name -t 26 dir20/archive1.tar dir3/script2.py dir2 dir5;
     ./$script_name -t 26 dir20/archive2.tar.gz dir/test.sh dir/file2 dir4;
     ./$script_name -t 26 dir20/archive3.tar.bz2 dir18/sym.sh dir18/dir2/.htaccess dir;
     ./$script_name -t 26 dir20/archive4.tar.lz dir19/files dir5;
     ./$script_name -t 26 dir20/archive5.tar.lzma dir19/files/file1.txt dir16;
     ./$script_name -t 26 dir20/archive6.tar.xz dir19/files/file2.txt dir;
     ./$script_name -t 26 dir20/archive7.tar.Z dir19/files/file3.txt dir2;
     ./$script_name -t 26 dir20/archive8.tlz dir19 dir dir5;
     ./$script_name -t 26 dir20/archive9.tbz2 dir19 dir18;

 27) Recursively copy the directory structure from the specified directory
     (excluding files).

     This task involves recursively copying the directory structure (excluding files)
     from a specified source directory to a destination directory. The script ensures
     that the destination directory is created if it does not already exist.

     The script utilizes the find command with the '-type d' option to identify all
     directories within the source directory. For each directory found, the 'mkdir -vp'
     command is used to create the corresponding directory structure in the destination
     directory. The -p option ensures that the command creates parent directories as
     needed, and it avoids any errors if the destination directory already exists.

     By executing this task, the directory hierarchy from the source directory will be
     replicated in the destination directory without copying any files, making it ideal
     for duplicating the structure of complex directory trees.

     Example: ./$script_name -t 27 dir6 dir20;

 28) Display a list of all system users (names only) in alphabetical order.

     This task aims to provide a list of all system users, displaying only their
     usernames, in alphabetical order. The list is retrieved from the 'etc/passwd' file,
     which contains information about all users on the system.

     By using the 'cut' command to extract the usernames and the 'sort' command to
     arrange them alphabetically, the output presents a concise list of all system
     users' names for easy reference.

     Example: ./$script_name -t 28;

 29) Display a list of all system users sorted by ID, in the format: login id.

     This task aims to provide a sorted list of all system users along with their
     corresponding user IDs (UIDs). The list is retrieved from the '/etc/passwd' file,
     which contains information about user accounts on the system.

     The 'cut' command is used to extract the login names and UIDs from the file,
     and then the 'sort' command is used to arrange the users based on their UIDs
     in ascending numerical order. The final output displays each user's login name
     followed by their corresponding UID, making it easier to identify and manage
     system users.

     Example: ./$script_name -t 29;

 30) Display a list of all system users (names only) sorted by ID in reverse order.

     Task 30 aims to display a list of all system users (names only) sorted by their
     User IDs (UIDs) in reverse order. The command extracts the user names and UIDs from
     the '/etc/passwd' file, sorts the list based on the UIDs in reverse numeric order,
     and finally displays the sorted list of user names.

     This allows you to see the system users with the highest UIDs first and the lowest
     UIDs last.

     Example: ./$script_name -t 30;

 31) Display all users who do not have the right to log in or do not have a login shell
     in the system. (two commands)

     This task involves using two commands to display two lists of users:

     a) Users without login rights:
        This command searches the '/etc/shadow' file and identifies users who do not
        have the right to log in.

     b) Users without a login shell:
        This command searches the '/etc/passwd' file and identifies users who do not
        have a login shell specified.

     The lists will provide information about users who are restricted from logging into
     the system or do not have a valid login shell defined.

     NOTE: The information from these commands is important for managing user access and
     system security.

     Example: ./$script_name -t 31;

 32) List all users who have or do not have a terminal (bash, sh, zsh, etc.).
     (Two commands).

     This task involves two commands to list users based on whether they have a terminal
     shell (such as bash, sh, zsh, etc.) or do not have a terminal shell (using
     '/bin/false' or '/sbin/nologin' shells).

     The first command lists users with a terminal, while the second command lists users
     without a terminal. The information is extracted from the '/etc/passwd' file.

     Example: ./$script_name -t 32;

 33) Download all links to href resources on a web page.
     a) Use curl and wget. Download in parallel.
     b) Provide recommendations for usage.

     This task allows you to download all links to href resources found on a specified
     web page. The script uses curl and wget to download the resources in parallel,
     making the process more efficient.

     Before proceeding, it checks the URL to ensure it returns an HTTP status code of
     200 using 'curl' and '$?'. Additionally, it creates the output directory if it does
     not exist.

     Explanation of Commands Used:

     'curl -s': Retrieves the content of the specified URL in silent mode (-s).
     'grep -oE 'href="[^"]+"'': Extracts all occurrences of href attributes with their
     corresponding URLs from the HTML content.
     'cut -d'"' -f2': Extracts only the URLs from the href attributes using the
     double-quote delimiter (").
     'xargs -P 10 -I {}': Runs wget in parallel with up to 10 instances using {} as the
     placeholder for each URL.
     'wget -nv -P "$output_dir" "{}"': Downloads the resources with the URL provided by
     {}, displaying no output (-q) and saving them in the specified output directory
     ('-P "$2"').

     Before downloading, the script performs the following checks:

     Verifies the URL's status code using curl and '$?' to ensure it returns a 200
     status code. Creates the output directory specified by the user if it does not
     exist.

     This task allows you to efficiently download resources from a web page, making it
     easier to access all the necessary href links in one go.

     NOTE: Remember to use this feature responsibly and avoid overloading the web server
     with too many simultaneous requests.

     Example: ./$script_name -t 33 www.columbia.edu/~fdc/sample.html dir21;

 34) Terminate processes that have been running for more than 5 days.

     This task allows you to terminate processes that have been running for more than 5
     days. It provides two options for terminating such processes: 'a' and 'b'.

     Option 'a' (killall):
     This option uses the killall command to find and terminate non-critical processes
     that have been running for more than 5 days, excluding specific system processes
     like systemd, sshd, and kworker.

     Option 'b' (no ps / killall):
     This option achieves the same task without relying on the ps and killall commands.
     It iterates through the /proc directory to find processes that meet the 5-day
     criteria and then terminates them using the kill -9 command. Similarly, it excludes
     specific system processes.

     IMPORTANT: Using these options may result in the termination of processes that have
     been running for more than 5 days, except for specific system processes. Use with
     caution.

     Examples:
     ./$script_name -t 34 a
     ./$script_name -t 34 b

 35) Delete files with a certain extension if there is no corresponding file with a
     different extension in the same directory ( e.g. *.txt & *.jpeg).

     This task allows you to delete files of a specified extension if there is no
     corresponding file with a different extension in the same directory. The script
     ensures that you provide three arguments: the target directory, the file type to
     delete, and the file type to compare.

     The script handles user input for file types to delete and compare. You can specify
     file types either with or without a leading dot (e.g., .txt or txt). The script will
     automatically handle the input appropriately by adding a wildcard * before the
     extension.

     IMPORTANT: You should avoid using wildcards (e.g., *) when specifying file types as
     arguments, especially when calling the script from the command line. If you need to
     use wildcards, make sure to quote the file type arguments to prevent shell expansion.
     For example:
     Correct: ./$script_name -t 35 path/to/dir1 jpeg txt; or
              ./$script_name -t 35 path/to/dir1 .jpeg .txt;

     Incorrect: ./$script_name path/to/dir1 *.jpeg *.txt;

     Before performing the deletion, the script checks if there are any files matching
     the provided file types in the directory. If no files are found, it will display
     appropriate messages and exit.

     IMPORTANT: If the script does not find corresponding files with another extension
     in the subdirectories (however, they can be found in the parent directory), it will
     still delete all files with the specified type as the second argument from the
     subdirectories.

     The script uses a function called 'file_type' for file type handling, which
     automatically adds the necessary wildcard based on the input. It then employs the
     find command to search for files with the specified file type to delete.

     For each file found, the script checks if a corresponding file with the compare file
     type exists in the same directory. If not, it will delete the file.

     After the deletion process, the script displays the directory information before and
     after the operation, along with a message indicating which files were deleted.

     Examples:
     ./$script_name -t dir30 .jpeg txt;
     ./$script_name -t dir31 txt .jpeg;

 36) Find your IP address using the command line.

     This task allows you to find either your private or public IP address through the command line. Two options are allowed:
        - 'pr': Find your private IP address using the 'hostname' command.
        - 'pb': Find your public IP address using the 'curl' command and the
        'checkip.amazonaws.com' service.

     The script performs the appropriate command and displays the resulting IP address
     based on your chosen option.

     You can select either option as an argument while running the script.

     Examples:
     ./$script_name -t 36 pr      (Finds your private IP address)
     ./$script_name -t 36 pb      (Finds your public IP address)

 37) Get all IP addresses from a text file.

     In Task 37, the objective is to extract and display all valid IPv4 addresses
     present in a given file. The task operates in silent mode by default, providing
     a concise output that shows only the valid IPv4 addresses found in the file.

     By default, when you execute the script with Task 37 and specify the file, it will
     display the contents of the file followed by the valid IPv4 addresses found in the
     file. If no valid IPv4 addresses are found, it will indicate that there are no
     valid IP addresses in the file.

     NOTE: You can also choose the 'verbosity mode' ('-v') as an additional option. When
     you specify '-v' along with the file, it will provide a more detailed output,
     validating each line in the file to check whether it contains a valid IPv4 address.

     For each valid address, it will indicate whether it is a private or public IPv4
     address.

     To summarize, you can use Task 37 with the following options:
     - Without '-v': Silent mode (default), showing only valid IPv4 addresses in the
     file.
     - With '-v': Verbosity mode, validating and displaying the classification of each
     IP address (private or public).

     Examples:
     ./$script_name -t 37         (Default mode, IPs only)
     ./$script_name -t 37 -v      (Verbosity mode, IPs with info)

 38) [x] Check hosts from the command line input or file by ping or nmap utility.

     Task 38 provides a versatile and user-friendly solution to check the activity of
     multiple IP addresses and networks using the 'ping' or 'nmap' utility. It supports
     various features, including validating IPs and networks, specifying IP ranges using
     brace expansion, and the ability to check active hosts only.

     NOTE: If you choose to use 'nmap' but it's not found on the system, the script
     should prompt you to install it. This ensures that the necessary utility ('nmap') is
     available for scanning the hosts effectively.

     Features:
     - Supported Networks and Prefixes: The script supports all three major private
       networks (10.0.0.0/8, 172.16.0.0/16, and 192.168.0.0/16) and their corresponding
       prefixes (e.g., /24) for checking active hosts.

     - IP and Network Validation: The script validates the provided IP addresses and
       networks to ensure only valid entries are processed.

     - Ping and Nmap: The script allows users to choose between using 'ping' or 'nmap'
       to check host activity.

     - Active and Inactive Hosts: By default, the script provides information about both
       active and inactive hosts. It can also show active hosts only using the "-a" flag.

     - Warning for Large Scans: The script issues a warning if a large list of IPs or
       networks is being scanned, providing users the option to proceed or not.

     - Handling No Active Hosts: If no active hosts are found during the scan, the script
       notifies the user.

     - Error Handling for No Valid IPs: The script alerts the user if no valid IP addresses 
       or networks are provided.

     - Pipe and Chaining Support: The script can be used with scripting pipes and
       chaining, allowing for flexible use with other commands.

     Examples:
     - Ping Multiple IPs/Networks:
       ./$script_name -t 38 192.168.0/24 10.255.255.255 1.1.1.1 192.168.0.{1..4}
       ./$script_name -t 38 255.255.255.255 172.16.0.0 221.1.1.2 1.1.1.1 -a

     - No Valid IPs Passed:
       ./$script_name -t 38 no valid IP addresses or networks
       ./$script_name -t 38 no valid from a file \$(cat dir4/hi.txt) -a

     - Nmap Multiple IPs/Networks:
       ./$script_name -t 38 192.168.0.0/24 10.255.255.255 221.1.1.2 1.1.1.1 nmap

     - No Active Hosts Found by Nmap:
       ./$script_name -t 38 255.255.255.255 172.16.255.0 221.1.1.2 221.1.1.87 nmap -a

     - Checking Hosts and Networks by Ping from a File:
       ./$script_name -t 38 \$(cat dir4/hosts-server.txt)

     - Checking Active Hosts and Networks by Ping from a File:
       ./$script_name -t 38 \$(cat dir4/hosts-server.txt) -a

     - Scanning Local Network by Nmap:
       ./$script_name -t 38 \$(ip addr | grep 'inet ' |
       cut -d ' ' -f 6 | head -2 | tail -1) -nmap

     The script empowers users to efficiently manage and verify the activity of multiple
     hosts and networks, making it a valuable tool for network administrators and
     security professionals.

     Additionally, the comprehensive error handling and interactive warnings ensure a
     smooth and reliable experience for the user. For further information and
     advanced usage, please refer to the script's Scripting & Pipes manual chapters.

     IMPORTANT: Ensure that you run this task at the end of the chain when using the
     script in a chain of tasks. Refer to the 'Chaining' section for more details.

     Examples:
     ./$script_name -t 38 172.16.255.0/24 192.168.0.1 8.8.8.8
     ./$script_name -t 38 172.16.255.10/24 192.168.0.2 1.1.1.2 -a
     ./$script_name -t 38 10.16.255.0/8 192.168.0.100 8.8.8.8 nmap
     ./$script_name -t 38 172.16.255.10/16 1.1.1.2 \$(cat dir4/hosts*) nmap -a

 39) Filter the output of Task 36 to show uplink interfaces with assigned IPs.

     Task 39 filters the output of Task 36 to display only the uplink interfaces with
     assigned IP addresses. It uses regular expressions to extract the IP addresses from
     the output of Task 36. Then, it checks if any private IP addresses are assigned.

     If no private IP addresses are found, it displays a message indicating that no
     private IP address is assigned. Otherwise, it looks up the interfaces corresponding
     to the private IP addresses and prints the interface names along with their
     respective IP addresses.

     Example:
     ./$script_name -t 39

 40) Get all subdomains from an SSL certificate.

     Task 40 is a Bash function that extracts and displays subdomains from an SSL
     certificate using OpenSSL. If no subdomains are found, it will show a message
     indicating the absence of subdomains in the certificate.

     Example:
     ./$script_name -t 40 dir50/example.crt

 41) Extract the file path, name, and extension from a given file path.

     This task involves creating a Bash function that takes a file path string as an
     argument. The function then extracts the directory path, file name (without
     extension), and file extension from the given string and displays them as output.

     If the file has no extension, it will indicate this in the output. The function
     uses 'dirname', 'basename', and parameter expansion to perform the extraction.

     Example:
     ./$script_name -t 41 dir4/files/file2.txt

 42) Delete files in a specified directory and its subdirectories based on their length
     and type.

     This task enables you to delete files within a specified directory and its
     subdirectories based on two criteria: their file type and size. You need to provide
     three arguments for this task:

     - Directory: The path to the directory in which the deletion will be performed.

     - Type: The file type you want to delete. You can use wildcards (*, ?) to match
     multiple file names, but ensure that you quote them to avoid unexpected script
     behavior.

     - Size: The file size threshold for deletion. Files larger than the specified size
     will be deleted.

     IMPORTANT: The script supports wildcards in the type argument. However, it is
     essential to enclose them in double quotes (e.g. ".txt" or '.txt') to prevent the
     shell from expanding them prematurely and potentially causing unintended file
     deletions.

     The script will validate the provided size to ensure it follows a specific format.
     Once the size is validated, it will use the 'find' command to identify and delete
     files that match the specified type and size criteria.

     If files are deleted, their names will be displayed as output. If no files meet the
     criteria for deletion, the script will display the message:
     "No files deleted. Check file size."

     CAUTION: Ensure you understand the consequences of deleting files before running
     this task. Verify the provided arguments to avoid accidental data loss or removal
     of essential files.

     Example:
     ./$script_name -t 42 dir55 "*.txt" 8192c

 43) Create files in a designated directory using a file that contains file names and
     their corresponding hash IDs, and add the hash ID entry to each file.

     This task involves reading a file that contains a list of file names and their
     associated hash IDs (4-byte identifiers). For each entry in the file, a new file is
     created in the specified directory, and the corresponding hash ID is added to the
     file. This process ensures that each file is uniquely identified by its hash value.

     The task also provides visual feedback by displaying the results, making the
     process more interactive and informative.

     Example:
     ./$script_name -t 43 dir55/list.txt dir77

 -d, --demo    Demo Mode. See above.

 -h, --help    Display this help

EOF
}

### Aux functions ##################################################################
# Check user input: file
check_file() {
    # Check if the file exists
    if [ ! -f "$1" ]; then
        echo "File '$1' does not exist."
        exit 1
    fi
}

# Check user input: directory
check_dir() {
    # Check if the directory exists
    if [ ! -d "$1" ]; then
        echo "Directory '$1' does not exist."
        exit 1
    fi
}

# Check user input: file or directory
check_file_dir() {
    # Check if the directory exists
    if [ ! -e "$1" ]; then
        echo "File or directory '$1' does not exist."
        exit 1
    fi
}

# Check user input: symbolic link
check_slink() {
    # Check if the symbolic link exists
    if [ ! -L "$1" ]; then
        echo "Symbolic link '$1' does not exist."
        exit 1
    fi
}

# Check user input: create directory if it does not exist
test_create_dir() {
    # Check user input: directory
    if [ ! -d "$1" ]; then
        mkdir -p "$1"
        echo "Directory created: '$1'"
    fi
}

# Check user input: create directory for a file if it does not exist
test_create_dirname() {
    # Check user input: directory
    if [ ! -d "$(dirname $1)" ]; then
        mkdir -p "$(dirname $1)"
        echo "Directory created: '$(dirname $1)'"
    fi
}

# Check if utility is installed
check_util() {
    command -v "$1" >/dev/null 2>&1
}

# Install utility using yum, apt-get, or dnf
install_util() {
    check_util yum && sudo yum install "$1" -y && return
    check_util dnf && sudo dnf install "$1" -y && return
    check_util apt-get && sudo apt-get install "$1" -y && return

    # Check if 'epel' repository is installed on Amazon Linux
    if check_util amazon-linux-extras; then
        sudo amazon-linux-extras install epel -y && sudo yum install "$1" -y && return
    fi

    echo "Error: no supported package manager found"
    return 1
}

# Ask user whether to install the utility
custom_install() {
    while true; do
        read -r -p "Want to install $1? [y/n] " input
        case $input in
            [yY])
                install_util "$1"
                break
                ;;
            [nN])
                break
                ;;
            *)
                echo "Answer (y) for yes or (n) for no."
                ;;
        esac
    done
}

# Display detailed information about a specific file or directory contents
list_dir_or_file_info() {
    # Check the directory
    if [ -d "$1" ]; then
        # Header with a static line
        message_with_animated_or_static_bar \
        "Directory '$1' contents:" 55 - 0
    # Check the file
    elif [[ -f "$1" && ! -L "$1" ]]; then
        # Header with a static line
        message_with_animated_or_static_bar \
        "File '$1' information:" 55 - 0
    # Check the symbolic link
    elif [ -L "$1" ]; then
        # Header with a static line
        message_with_animated_or_static_bar \
        "Symbolic link '$1' information:" 55 - 0
    else
        # Header with a static line
        message_with_animated_or_static_bar \
        "'$1' information:" 55 - 0
    fi
    ls -Rl "$1"
}

# Retry counter function
retry() {
    # Set the healthcheck function and counter
    local function="$@"
    local count=0
    local max=5
    local sleep=2

    while :; do
        $function && break
        if [ "$count" -lt "$max" ]; then
            ((count++))
            echo "Command failed, retrying after $sleep seconds... $count/$max"
            sleep "$sleep"
            continue
        fi
        echo "Command failed, out of retries."
        exit 1
    done
}

# Healthcheck function
healthcheck() {
    # URL check
    curl -ILs "$1" | grep " 200 " >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "URL hasn't responded 200"
        return 1
    fi
    echo "URL responded 200"
}


# Header with an animated or static line
message_with_animated_or_static_bar() {
    echo "$1"
    for i in $(seq "$2"); do
        printf "$3"
        sleep "$4"
    done
    echo
}

### Aux functions End ##############################################################

### Argument check in the case statement code ######################################
# 'Insufficient arguments' message
wrong_arg_msg() {
    # Display help and the 'Insufficient arguments' message
    echo
    man | grep -A 10 -E "^ *$task\)" &&
    echo -e "\n\e[31m     Insufficient arguments for option $task. Refer to the manual for more: ./$script_name -h\e[0m"
    exit 1
}

# Check user input: command
arg_check_input() {
    if [[ "$1" != "$2" && "$1" != "$3" ]]; then
        echo "Invalid command: $1. Must be '$2' or '$3'."
        exit 1
    fi
}

# Argument check if none
arg_check0() {
    if [ -z "$1" ]; then
        wrong_arg_msg
    fi
}

# Argument check if less than 2
arg_check2() {
    if [ "$1" -lt 2 ]; then
        wrong_arg_msg
    fi
}

# Argument check if less than 3
arg_check3() {
    if [ "$1" -lt 3 ]; then
        wrong_arg_msg
    fi
}

# Argument check if less than 4
arg_check4() {
    if [ "$1" -lt 4 ]; then
        wrong_arg_msg
    fi
}

### Argument check End #############################################################

### Task functions #################################################################
# Task 1
task1() {
    echo
    # Check user input: create directory for a file if it does not exist
    test_create_dirname "$1"

    cut -d: -f1,3 /etc/group | sort -t: -u -k2 |
    tee "$1"
    echo
    # File info
    list_dir_or_file_info "$1"
}

# Task 2
task2() {
    echo
    # Check user input: directory
    check_dir "$1"

    result=$(find $1 -user $2 -group $3 -ls)

    if [ -z "$result" ]; then
        echo "No files / directories found for '$2':'$3' in the directory '$1'."
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "Files / directories found for '$2':'$3' in the directory '$1':" 55 - 0
        echo "$result"
    fi
}

# Task 3
task3() {
    echo
    # Check user input: directory
    check_dir "$1"

    result=$(find "$1" \( -name "*.sh" -o -name "*.py" -o -name "*.rb" -o -not -name "*.*" \) \
        -exec bash -c 'file "$0" | grep -q "shell script\|Python script\|Ruby script" \
        && echo "$0"' {} \;)

    executables=$(find "$1" -type f -executable)

    if [ -z "$result" ] && [ -z "$executables" ]; then
        echo "No script files or executables found in directory '$1'."
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "Script files found in the directory '$1':" 55 - 0
        echo "Script files:"
        echo "$result"
        echo
        echo "Executables:"
        echo "$executables"
    fi
}

# Task 4
task4() {
    echo
    # Check user input: directory
    check_dir "$1"

    result=$(find "$1" -user "$2" \( -name "*.sh" -o -name "*.py" -o -name "*.rb" -o -not -name "*.*" \) \
        -exec bash -c 'file "$0" | grep -q "shell script\|Python script\|Ruby script" \
        && echo "$0"' {} \;)

    executables=$(find "$1" -user "$2" -type f -executable)

    if [ -z "$result" ] && [ -z "$executables" ]; then
        echo "No script files or executables found for the user '$2' in the directory '$1'."
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "Script files or executables found for the user '$2' in the directory '$1':" 55 - 0
        echo "Script files:"
        echo "$result"
        echo
        echo "Executables:"
        echo "$executables"
    fi
}

# Task 5
task5() {
    echo
    # Check user input: directory
    check_dir "$1"

    # File type user input handling
    if [[ "$2" =~ "." ]]; then
        file_type="*$2"
    else
        file_type="*.$2"
    fi

    # A word or phrase search for a specific file type in a specified directory
    result=$(find "$1" -type f -name "$file_type" -exec grep -H "$3" {} +)

    if [ -z "$result" ]; then
        echo "No '${file_type:1}' files found containing '$3' in the directory '$1'."
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "'${file_type:1}' files found containing '$3' in the directory '$1':" 55 - 0
        echo "$result"
    fi
}

# Task 6
# Hash function name & value output handling
get_hash_options() {
    case "$1" in
        cksum) echo "CRC32" 8 ;;
        md5sum) echo "MD5" 32 ;;
        sha1sum) echo "SHA-1" 40 ;;
        sha224sum) echo "SHA-224" 56 ;;
        *) echo "$1" 56 ;;
    esac
}

# Task 6 main function
task6() {
    echo
    # Function variables
    hash_func=${@: -1}  # Last argument is the hash function
    directories=${@:1:$(($# - 1))}  # All arguments except the last one are directories

    # Check user input: directories
    for dir in $directories; do
        check_dir "$dir"
    done

    # Get a Hash function value length & name for the output
    func=$(get_hash_options "$hash_func" | cut -d ' ' -f 1)
    hash_length=$(get_hash_options "$hash_func" | cut -d ' ' -f 2)

    previous_hash=""  # Variable to store the previous hash value

    # Header with a static line
    # Output message
    message_with_animated_or_static_bar \
        "'$func' duplicate file check has been performed in the following directories: '${directories// /, }'" 55 - 0

    # Driver
    find $directories -type f -printf "%s\n" | sort -rn | uniq -d |
    while read -r size; do
        find $directories -type f -size "${size}c" -print0 |
        xargs -0 $hash_func | sort | uniq -D -w $hash_length |
        while read -r line; do
            hash=${line:0:$hash_length}  # Extract the hash value from the line

            # Echoing a tittle before each file group
            if [ "$hash" != "$previous_hash" ]; then
                # Hash function name in the output
                echo
                message_with_animated_or_static_bar \
                    "Duplicate file group (size: "$size"B; checked by: "$func"):" 55 - 0
            fi

            # Removing hash values from output
            if [[ $line =~ ^([0-9]+)\ ([0-9]+)\ (.+)$ ]]; then
                echo "${BASH_REMATCH[3]}"  # Remove the CRC32 hash
            else
                echo "${line//$hash  /}"  # Remove the hash
            fi
            previous_hash=$hash  # Update the previous hash value
        done
    done
}

# Task 7
task7() {
    echo
    # Check user input: directory
    check_dir "$1"

    # User input file path handling
    check_file "$2"

    soft_links=$(find "$1" -lname "$(readlink -f "$2")" -o -lname "*$(basename "$2")")

    if [ -z "$soft_links" ]; then
        echo "No symbolic links found for the file '$2' in the directory '$1'."
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "Symbolic links found for the file '$2' in the directory '$1':" 55 - 0
        echo "$soft_links"
    fi
}

# Task 8
task8() {
    echo
    # Check user input: directory
    check_dir "$1"

    # Check user input: file
    check_file "$2"

    hard_links=$(find "$1" -samefile "$2")

    if [ -z "$hard_links" ]; then
        echo "No hard links found for the file '$2' in the directory '$1'."
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "Hard links found for the file '$2' in the directory '$1':" 55 - 0
        echo "$hard_links"
    fi
}

# Task 9
task9() {
    echo
    # Find all names of the file with the given inode
    names=$(find / -inum "$1" 2>/dev/null)

    if [ -z "$names" ]; then
        echo "No names found for the inode '$1'."
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "File names found for the inode '$1':" 55 - 0
        echo "$names"
    fi
}

# Task 10
task10() {
    echo
    # Find all names of the file with the given inode in all mounted sections
    names=$(find / -xdev -inum "$1" 2>/dev/null)

    if [ -z "$names" ]; then
        echo "No names found for the inode '$1' in all mounted sections."
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "File names found for the inode '$1' in all mounted sections:" 55 - 0
        echo "$names"
    fi
}

# Task 11
task11() {
    echo
    # Check user input: file
    check_file "$1"

    # Delete the file, including symbolic and hard links if any
    links=$(find / -xdev \( -samefile "$1" -o -lname "$(readlink -f "$1")" -o -lname "*$(basename "$1")" \) 2>/dev/null)
    output=$(find / -xdev \( -samefile "$1" -o -lname "$(readlink -f "$1")" -o -lname "*$(basename "$1")" \) \
        -exec rm -v {} + 2>/dev/null | wc -l)

    if [ "$output" -gt 1 ]; then
        # Header with a static line
        message_with_animated_or_static_bar \
            "File '$1' and its links have been deleted:" 55 - 0
        echo "$links"
    else
        echo "File '$1' has been deleted."
    fi
}

# Task 12
task12() {
    echo
    # Check user input: directory
    check_dir "$1"
    # Check user input: file
    check_file $1/$2 | grep -E "not exist." >/dev/null &&
    echo "No files matching '$2' found in the directory '$1'." &&
    exit 1

    # Files to change access rights
    # Header with a static line
    message_with_animated_or_static_bar \
        "'$2' files to change access rights to '$3':" 55 - 0
    ls -l $1/$2

    # Recursively change access rights to files in the specified directory
    find "$1" -type f -name "$2" -exec chmod "$3" {} +

    # Output the files with changed access rights
    # Header with a static line
    echo
    message_with_animated_or_static_bar \
        "Access rights changed to '$3' for files matching '$2' in the directory '$1':" \
            55 - 0
    ls -l $1/$2
}

# Task 13
task13() {
    echo
    # Check user input: directory
    check_dir "$1" && check_dir "$2"

    # Using diff
    diff_output=$(diff -rq "$1" "$2" | grep "Only in" |
    while read line; do
        echo "${line/Only in /}"
    done | tr ':' '/' | tr -d ' ')

    if [ -z "$diff_output" ]; then
        echo "No differences found between the directories '$1' and '$2'."
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "Differences found between the directories '$1' and '$2':" 55 - 0
    fi

    # Split the diff_output into pairs
    pairs=()
    while IFS= read -r line; do
        pairs+=("$line")
    done <<< "$diff_output"

    # Iterate over the pairs and perform diff
    prev_file=""
    for file in "${pairs[@]}"; do
        if [ -n "$prev_file" ]; then
            echo "Difference in files: $prev_file and $file"
            diff "$prev_file" "$file"
        fi
        prev_file="$file"
    done
}

# Task 14
task14() {
    echo
    # Getting MAC addresses
    ip link show | grep -v 'lo:' |
    grep -oP '(?<=link/ether )\S+|\d+: \K.*(?=:)' |
    xargs -n2 printf '%s: %s\n'
}

# Task 15
task15() {
    echo
    # Getting users logged in
    who | cut -d ' ' -f 1 | sort
}

# Task 16
task16() {
    echo
    # Getting a list of active connections
    ss -ant | tail -n +2 | grep -o '^[^ ]*' | sort | uniq -c |
    while read count type; do
        echo "$type $count";
    done
}

# Task 17
task17() {
    echo
    # Check user input: symbolic link
    check_slink "$2"

    # Header with a static line
    message_with_animated_or_static_bar \
        "Symbolic link '$2' to be re-assigned:" 55 - 0
    ls -l "$2"

    # Re-assigning symbolic link
    echo
    message_with_animated_or_static_bar \
        "Re-assigning to '$1':" 55 - 0
    ln -svf "$1" "$2"

    echo
    # Header with a static line
    message_with_animated_or_static_bar \
        "Symbolic link '$2' has been re-assigned to '$1':" 55 - 0
    ls -l "$2"
}

# Task 18
task18() {
    echo
    # Function variables
    dir_path=${@: -1}  # Last argument is the directory
    files=${@:1:$(($# - 1))}  # All arguments except the last one are files

    # Check user input: files
    for file in $files; do
        check_file "$file"
    done

    # Check user input: create directory if it does not exist
    test_create_dir "$dir_path"

    # Header with a static line
    # Output message
    message_with_animated_or_static_bar \
        "Creating symbolic links in the directory '$dir_path' for the following files: '${files// /, }'" 55 - 0

    # Create symbolic links to the specified files in the specified directory
    for file in $files; do
        echo "Realative symbolic link created:"
        ln -rsv "$file" "$dir_path/$(basename "$file")"
    done
}

# Task 19
# Task 19-21 copying aux function
cp_rsync_copy() {
    # Copy driver arguments:
    # $1 - coomand; $2 - source dir; $3 - destination dir;
    # $4 - cp option; $5 - rsync option

    # Handling the source dir user input
    src_dir="${2%\/*}" # Remove the "/." part if needed

    # Check user input: the whole directory or its content only
    if [[ ! "$2" =~ [.]+$ ]]; then
        # Header with a static line
        message_with_animated_or_static_bar \
            "Preparing directories. Source: '$2', destination: '$3':" 55 - 0
        ls -Rl "$2" "$3"
        # Perform the copy operation based on the specified command
        if [ "$1" = "cp" ]; then
            echo
            # Header with a static line
            message_with_animated_or_static_bar \
                "Copying directory '$2' to '$3' using command: '$1' with option: '$4':" 55 - 0
            cp "$4" "$2" "$3"
        elif [ "$1" = "rsync" ]; then
            echo
            # Header with a static line
            message_with_animated_or_static_bar \
                "Copying directory '$2' to '$3' using command: '$1' with option: '$5':" 55 - 0
            rsync "$5" "$2" "$3"
        fi
    else
        # Header with a static line
        message_with_animated_or_static_bar \
            "Preparing directories, source: '$src_dir', destination: '$3':" 55 - 0
        ls -Rl "$src_dir" "$3"
        # Perform the directory contents copy operation based on the specified command
        if [ "$1" = "cp" ]; then
            echo
            # Header with a static line
            message_with_animated_or_static_bar \
                "Copying directory '$src_dir' contents to '$3' using command: '$1' with option: '$4':" \
                    55 - 0
            cp "$4" "$2" "$3"
        elif [ "$1" = "rsync" ]; then
            echo
            # Header with a static line
            message_with_animated_or_static_bar \
                "Copying directory '$src_dir' contents to '$3' using command: '$1' with option: '$5':" \
                    55 - 0
            rsync "$5" --exclude='$2' $(realpath "$2")/ $(realpath "$3")
        fi
    fi

    echo
    # Header with a static line
    message_with_animated_or_static_bar \
        "Destination directory '$3' contents:" 55 - 0
    ls -Rl "$3"
}

# Task 19 main function
task19() {
    echo
    # Check user input: command
    arg_check_input "$1" cp rsync

    # Check user input: source directory
    check_dir "$2"

    # Check user input: create destination directory if it does not exist
    test_create_dir "$3"
    # Copy driver:
    # $1 - coomand; $2 - source dir; $3 - destination dir;
    # $4 - cp option; $5 - rsync option
    cp_rsync_copy "$1" "$2" "$3" -Rav -HAXSravz
}

# Task 20
task20() {
    echo
    # Check user input: command
    arg_check_input "$1" cp rsync

    # Check user input: source directory
    check_dir "$2"

    # Check user input: create destination directory if it does not exist
    test_create_dir "$3"
    # Copy driver:
    # $1 - coomand; $2 - source dir; $3 - destination dir;
    # $4 - cp option; $5 - rsync option
    cp_rsync_copy "$1" "$2" "$3" -Rdv -Hlrvz
}

# Task 21
task21() {
    echo
    # Check user input: command
    arg_check_input "$1" cp rsync

    # Check user input: source directory
    check_dir "$2"

    # Check user input: create destination directory if not exist
    test_create_dir "$3"
    # Copy driver:
    # $1 - coomand; $2 - source dir; $3 - destination dir;
    # $4 - cp option; $5 - rsync option
    cp_rsync_copy "$1" "$2" "$3" -Rav -ravz
}

# Task 22
# Task 22-23 symbolic link handling aux function
symlink() {
    # Function arguments:
    # $1 - source dir; $2 - command 1; $3 command 2
    while IFS= read -r link; do
        if [ -L "$link" ]; then
            # Get the link target
            target=$(readlink "$link")
            # Check if it's an absolute path
            if [ "${target#/}" != "$target" ]; then
                # Convert to a relative path
                rel_path=$(realpath --relative-to="$(dirname "$link")" "$target")
                eval "$2"
            else
                # Convert to an absolute path
                abs_path=$(realpath "$link")
                eval "$3"
            fi
        fi
    done < <(find "$1" -type l)
}

# Task 22 main function
task22() {
    echo
    # Check user input: directory
    check_dir "$1"

    # Dir info
    list_dir_or_file_info "$1"
    echo

    # Convert relative symbolic links to absolute in the specified directory
    # Header with a static line
    message_with_animated_or_static_bar \
        "Converting relative symbolic links in the directory '$1' to absolute:" 55 - 0
    symlink "$1" continue 'ln -svf "$abs_path" "$link"'
    echo
    # Dir info
    list_dir_or_file_info "$1"
}

# Task 23
task23() {
    echo
    # Check user input: directory
    check_dir "$1"

    # Dir info
    list_dir_or_file_info "$1"
    echo

    # Convert absolute symbolic links to relative in the specified directory
    # Header with a static line
    message_with_animated_or_static_bar \
        "Converting absolute symbolic links in the directory '$1' to relative:" 55 - 0
    symlink "$1" 'ln -svf "$rel_path" "$link"' continue
    echo
    # Dir info
    list_dir_or_file_info "$1"
}

# Task 24
task24() {
    echo
    # Check user input: directory
    check_dir "$1"

    # Dir info
    echo "Directory '$1' contents:"
    ls -LRl "$1"
    echo

    # Check broken links if any
    broken_links=$(find "$1" -xtype l -print -o -type d)
    if [ -z "$broken_links" ]; then
       echo "No broken links found in the directory '$1'."
       exit 1
    fi

    # Find and delete broken links in the specified directory and its subdirectories
    # Header with a static line
    message_with_animated_or_static_bar \
        "Finding and deleting broken links in the directory '$1' and its subdirectories:" \
            55 - 0
    find "$1" -xtype l -delete -print -o -type d -exec find {} -xtype l -delete -print \;
    echo
    # Dir info
    echo "Directory '$1' contents:"
    ls -LRl "$1"
}

# Task 25
# Task 25-26 aux function to check if 'lzip', 'bzip2', and
# 'compress' utilities are installed
check_lzip_compress_bzip2() {
    local missing_util=false

    if ! check_util lzip; then
        echo "lzip utility not found."
        custom_install "lzip"
        missing_util=true
    fi

    if ! check_util compress; then
        echo "compress utility not found."
        custom_install "ncompress"
        missing_util=true
    fi

    if ! check_util bzip2; then
        echo "bzip2 utility not found."
        custom_install "bzip2"
        missing_util=true
    fi
}

# Task 25-26 aux function to determine tar options based on archive file extension
get_tar_options() {
    case "$1" in
        *.tar.gz|*.tgz) echo "-tzvf" "-xzvf" "-cvzf" ;;
        *.tar.Z) echo "-tZvf" "-xZvf" "-cZvf" ;;
        *.tar.bz2|*.tbz2|*.tlz) echo "-tjvf" "-xjvf" "-cvjf" ;;
        *.tar|*.tar.lz) echo "-tvf" "-xvf" "-cvf" ;;
        *.tar.lzma|*.tlzma|*.tar.xz|*.txz) echo "-tvJf" "-xvJf" "-cvJf" ;;
        *) echo "Unsupported archive format: $1"; return 1 ;;
    esac
}

# Skipping parents echo output aux function
strip_echo() {
    # Function arguments:
    # $1 - archive file; $2 - destination dir;
    # $3 - strip-components option;
    # $4 - if equals '--all' all contents extracted;
    # ${@:4:$#} (all but three) - archive contents
    contents=${@:4:$#}

    # Check --strip-components option
    if [[ "$3" -eq 1 ]]; then
        parents="parent"
    else
        parents="parents"
    fi

    # Output handling
    if [[ "$3" -eq 0 && "$4" == "--all" ]]; then
        echo
        # --all option to extract all contents with --strip-components=0
        # Header with a static line
        message_with_animated_or_static_bar \
            "Extracting from '$1' to '$2' with option: '$tar_options':" 55 - 0
    elif [[ "$3" -ne 0 && "$4" == "--all" ]]; then
        echo
        # --all option to extract all contents skipping parents
        # Header with a static line
        message_with_animated_or_static_bar \
            "Extracting from '$1' to '$2' with option: '$tar_options' skipping '$3' $parents:" 55 - 0
    elif [[ "$3" -eq 0 && "$4" != "--all" ]]; then
        echo
        # Specified file or dir to extract with --strip-components=0
        # Header with a static line
        message_with_animated_or_static_bar \
            "Extracting '${contents// /, }' from '$1' to '$2' with option: '$tar_options':" 55 - 0
    else
        echo
        # Specified file or dir extract skipping parents
        # Header with a static line
        message_with_animated_or_static_bar \
            "Extracting '${contents// /, }' from '$1' to '$2' with option: '$tar_options' skipping '$3' $parents:" \
                55 - 0
    fi
}

# Task 25 main function
task25() {
    echo
    # Check if bzip2, lzip, and compress utilities are installed
    check_lzip_compress_bzip2
    # Check user input: archive file
    check_file "$1"
    # Test file for compatibility
    get_tar_options "$1" | grep -E "format:" &&
    # Message with static line
    message_with_animated_or_static_bar "Supported formats:" 55 - 0 &&
    man | grep -A 4 -E "^     \- \*.tar.gz," && exit 1

    # Get the appropriate tar options based on the archive file extension
    tar_test=$(get_tar_options "$1" | cut -d ' ' -f 1)
    tar_options=$(get_tar_options "$1" | cut -d ' ' -f 2)

    # Test & extract the specified directory/file from the archive to the destination
    if [[ "$4" == "--all" ]]; then
        echo
        # Test & extract with --all option to extract all contents
        # Header with a static line
        message_with_animated_or_static_bar \
            "Testing archive '$1':" 55 - 0
        tar "$tar_test" "$1" || exit 1
        # Check user input: create destination directory if it does not exist
        test_create_dir "$2"
        strip_echo "$@"
        tar "$tar_options" "$1" -C "$2" --strip-components="$3"
    else
        echo
        # Test & extract the specified directory/file
        # Header with a static line
        message_with_animated_or_static_bar \
            "Testing files within archive '$1':" 55 - 0
        # All arguments except the first three are files and/or dirs
        tar "$tar_test" "$1" ${@:4:$#} || exit 1
        # Check user input: create destination directory if it does not exist
        test_create_dir "$2"
        strip_echo "$@"
        tar "$tar_options" "$1" -C "$2" --strip-components="$3" ${@:4:$#} # All but 3
    fi
    echo
    # Dir info
    list_dir_or_file_info "$2"
}

# Task 26
task26() {
    echo
    # Check if bzip2, lzip, and compress utilities are installed
    check_lzip_compress_bzip2
    # Test file for compatibility
    get_tar_options "$1" | grep -E "format:" &&
    # Message with static line
    message_with_animated_or_static_bar \
        "Supported formats:" 18 - 0 &&
    man | grep -A 4 -E "^     \- \*.tar.gz," && exit 1

    # Check user input: Files & directories to be archived
    for archived in "${@:2:$#}"; do
        check_file_dir "$archived"
    done

    # Check user input: create directory for a file if it does not exist
    test_create_dirname "$1"
    # All arguments except the first one are files and/or directories to be archived
    contents=${@:2:$#}

    # Get the appropriate tar options based on the archive file extension
    tar_test=$(get_tar_options "$1" | cut -d ' ' -f 1)
    tar_options=$(get_tar_options "$1" | cut -d ' ' -f 3)

    # Create & test the archive
    # Message with static line
    echo
    message_with_animated_or_static_bar \
        "Creating archive '$1' from '${contents// /, }' with option: '$tar_options':" 55 - 0
    tar "$tar_options" "$1" ${@:2:$#} # All but 1
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "Permissions and attributes preserved for:" 55 - 0
    tar "$tar_test" "$1"
}

# Task 27
task27() {
    echo
    # Check user input: source directory
    check_dir "$1"

    # Dir info
    list_dir_or_file_info "$1"

    # Recursively copy the directory structure from the source directory
    # to the destination directory
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "Recursively copying the directory structure (excluding files) from '$1' to '$2':" 55 - 0
    find "$1" -type d -print -exec mkdir -vp "$2/{}" \;
}

# Task 28
task28() {
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "User list in alphabetical order:" 55 - 0
    cut -d ':' -f 1 /etc/passwd | sort
}

# Task 29
task29() {
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "User list sorted by UID:" 55 - 0
    cut -d: -f1,3 /etc/passwd | sort -t: -k2,2n | tr ':' ' '
}

# Task 30
task30() {
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "User list (names only) sorted by UID in reverse order:" 55 - 0
    cut -d: -f1,3 /etc/passwd | sort -t: -k2,2rn | cut -d: -f1
}

# Task 31
task31() {
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "Users without login rights:" 55 - 0
    sudo grep -E '^[^:]+:([^*!]|[!*]{2,}):' /etc/shadow | cut -d: -f1
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "Users without login shell:" 55 - 0
    grep -E '(/sbin/nologin|/bin/false)$' /etc/passwd | cut -d: -f1
}

# Task 32
task32() {
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "Users with a terminal:" 55 - 0
    grep -E '^.+:.+:.+:/[^:]+:(?:/usr)?/[^:]*/(ba)?sh$' /etc/passwd | cut -d':' -f1
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "Users without a terminal:" 55 - 0
    grep -E '(/sbin/nologin|/bin/false)$' /etc/passwd | cut -d: -f1
}

# Task 33
task33() {
    echo
    # URL check
    retry healthcheck "$1"
    # Check if the output directory exists, otherwise create it
    test_create_dir "$2"
    echo

    # Message with static line
    message_with_animated_or_static_bar \
        "Downloaded all resources from page '$1' to directory '$2':" 55 - 0
    # Using curl and wget in parallel to download href resources
    (curl -s "$1" | grep -oE 'href="[^"]+"' | cut -d'"' -f2 |
    xargs -P 10 -I {} wget -P "$2" -q "{}" &) & wait

    # Wait if the output directory is empty
    if [ -z "$(ls -A "$2")" ]; then
        sleep 3
    fi
    ls -Rl "$2"
}

# Task 34
# Task 34 vars
five_days_in_seconds=$((5 * 24 * 60 * 60))

# Terminate processes that have been running for more than 5 days using killall
task34_killall() {

    # Find process names of non-critical processes running for more than 5 days
    processes_to_terminate=$(ps -eo etimes,comm --no-headers |
    while read -r etimes cmd; do
        if [[ "$etimes" -ge "$five_days_in_seconds" && "$cmd" != "systemd" && \
        "$cmd" != "sshd" && "$cmd" != "kworker" ]]; then
            echo "$cmd"
        fi
    done)

    if [ -z "$processes_to_terminate" ]; then
        echo "No processes running for more than 5 days found."
        return
    fi

    # Terminate processes using killall with -o (older than) and
    # -e (exact matching) options
    for process_name in $processes_to_terminate; do
        killall -o 5d --older-than 5d -e $process_name
    done
}

# Terminate processes that have been running for more than 5 days using no ps/killall
task34_no_ps_killall() {
    found_processes=false  # Initialize a variable to track if any processes were found

    for proc_dir in /proc/[0-9]*; do
        pid=$(basename "$proc_dir")
        if [[ ! -r "$proc_dir/stat" || ! -r "$proc_dir/cmdline" ]]; then
            # Skip the process if the required /proc files or directories are missing or not readable
            continue
        fi

        etimes=$(grep -oE '^[0-9]+' "$proc_dir/stat" | cut -d ' ' -f 22)
        if ! [[ "$etimes" =~ ^[0-9]+$ ]]; then
            # Skip the process if etimes is not a valid integer
            continue
        fi

        cmd=$(tr '\0' ' ' < "$proc_dir/cmdline")

        if [[ "$etimes" -ge "$five_days_in_seconds" ]]; then
            # Exclude specific system processes
            if [[ "$cmd" != "systemd" && "$cmd" != "sshd" && "$cmd" != "kworker" ]]; then
                echo "Terminating process PID (task option '$1'): $pid, Command: $cmd"
                kill -9 "$pid"
                found_processes=true  # Set the flag to true if any process is found
            fi
        fi
    done

    # If no processes were found, display the message
    if ! $found_processes; then
        echo "No processes running for more than 5 days found."
    fi
}

# Task 34 main function
task34() {
    echo
    # Check user input: command
    arg_check_input "$1" a b

    # Task 34 options
    if [ "$1" == "a" ]; then
        # Message with static line
        message_with_animated_or_static_bar \
            "Using option '$1':" 55 - 0
        task34_killall
    elif [ "$1" == "b" ]; then
        # Message with static line
        message_with_animated_or_static_bar \
            "Using option '$1':" 55 - 0
        task34_no_ps_killall
    fi
}

# Task 35
# File type user input handling aux function
file_type() {
    if [[ "$1" =~ "." ]]; then
        del_file_type="*$1"
    else
        del_file_type="*.$1"
    fi

    if [[ "$2" =~ "." ]]; then
        compare_file_type="$2"
    else
        compare_file_type=".$2"
    fi
}

# Task 35, 42 file check aux function
check_files() {
    find "$1" -type f -name "$2"
}

# Task 35 main function
task35() {
    echo
    # Check user input: directory
    check_dir "$1"

    # File type user input handling: $2 $3
    file_type "$2" "$3"

    if [[ -z $(check_files "$1" "$del_file_type") ]]; then
        echo "No files matching '$del_file_type' found in the directory '$1'."
        exit 1
    fi

    if [[ -z $(check_files "$1" "*$compare_file_type") ]]; then
        echo "No files matching '*$compare_file_type' found in the directory '$1'."
        exit 1
    fi

    # Dir info
    list_dir_or_file_info "$1"
    echo

    # Message with static line
    message_with_animated_or_static_bar \
        "Deleting files of type '$del_file_type' without corresponding files of type '$compare_file_type' from the directory '$1':" \
            55 - 0
    echo

    # Delete files of type $2 without corresponding files of type $3
    find "$1" -maxdepth 1 -type f -name "$del_file_type" -exec sh -c '
        base_name="${0%.*}"
        compare_file="$base_name'$compare_file_type'"
        if [ ! -e "$compare_file" ]; then
            echo "Deleting: $0"
            rm "$0"
        fi
    ' {} \;

    echo
    # Dir info
    list_dir_or_file_info "$1"
}

# Task 36
task36() {
    echo
    # Check user input: command
    arg_check_input "$1" pr pb

    if [[ "$1" == "pr" ]]; then
        # Find private IP address using the 'hostname' command
        if [ -z "$(hostname -I)" ]; then
            # Message with static line
            message_with_animated_or_static_bar \
                "No Private IP address assigned" 55 - 0
        elif [ "$(hostname -I | wc -w)" -eq 1 ]; then
            # Message with static line
            message_with_animated_or_static_bar \
                "Private IP address:" 55 - 0
            hostname -I
        else
            # Message with static line
            message_with_animated_or_static_bar \
                "Private IP addresses:" 55 - 0
            hostname -I
        fi
    elif [[ "$1" == "pb" ]]; then
        # Find public IP address using the 'curl' command
        # Message with static line
        message_with_animated_or_static_bar \
            "Public IP address:" 55 - 0
        curl -s https://checkip.amazonaws.com/
    fi
}

# Task 37
# Task 37-39 IP regexps vars
ipv4_match='^([0-9]{1,3}\.){3}[0-9]{1,3}$'
priv_ipv4_match='^(10\.|172\.(1[6-9]|2[0-9]|3[01])\.|192\.168\.|127\.|169\.254\.)'
priv_network_10='^(10\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3}))/(8|16|24)$'
priv_network_172='^(172\.(1[6-9]|2[0-9]|3[01])\.([0-9]{1,3})\.([0-9]{1,3}))/(16|24)$'
priv_network_192='^(192\.168\.([0-9]{1,3})\.([0-9]{1,3}))/(16|24)$'

# Task 37 aux function to track active hosts or valid IPs
no_found() {
    if [ "$1" == "0" ]; then
        eval "$2"
    fi
}

# Task 37 aux function to track valid IPs with 2 vars
no_found2() {
    if [[ "$1" -eq 0 ]] && [[ "$2" -eq 0 ]]; then
        eval "$3"
    fi
}

# Task 37-38 aux function that outputs valid IP addresses with verbosity
# The function handles: $1 as file or command ('IPs for loop' in Task 38);
# $2 as private IP check; $3 as public IP check; $4 as non-valid IP check;
# $5-$7 as private network 10.0.0.0/8, 10.0.0.0/16 and 10.0.0.0/24 check;
# $9-${10} as private network 172.16.0.0/16 and 172.16.0.0/24 check;
# ${12}-${13} as private network 192.168.0.0/16 and 192.168.0.0/24 check;
# $4, $8, ${11}, and ${14}  as non-valid IP check (skipped as ':' in Task 38);
task37_silent_or_verbose() {
    valid_found=0  # Variable to track if any valid IP addresses are found
    non_valid_found=0  # Variable to track if any non-valid IP addresses are found

    # Read the file line by line
    while read -r line; do
        # Check if the line is a valid IPv4 address with digits and dots
        if [[ $line =~ $ipv4_match ]]; then
            # Split the line into octets using the dot as a delimiter
            IFS='.' read -ra octets <<< "$line"
            # Check if the values of the octets are within the valid range of 0-255
            if [[ ${octets[0]} -le 255 && ${octets[1]} -le 255 &&
            ${octets[2]} -le 255 && ${octets[3]} -le 255 ]]; then
                # Check if the IP address is private or public
                if [[ $line =~ $priv_ipv4_match ]]; then
                    eval "$2"
                    valid_found=1   # Trigger valid private IPs found
                else
                    eval "$3"
                    valid_found=1   # Trigger valid public IPs found
                fi
            else
                eval "$4"
                non_valid_found=2   # Trigger non-valid IPs found
            fi
        elif [[ $line =~ $priv_network_10 ]]; then
            if [[ ${BASH_REMATCH[2]} -le 255 && ${BASH_REMATCH[3]} -le 255 &&
            ${BASH_REMATCH[4]} -le 255 ]]; then
                if [[ ${BASH_REMATCH[5]} -eq 8 ]]; then
                    network="${line%.*.*.*}"
                    prefix="${BASH_REMATCH[5]}"
                    eval "$5"
                    valid_found=1   # Trigger valid private IPs found
                elif [[ ${BASH_REMATCH[5]} -eq 16 ]]; then
                    network="${line%.*.*}"
                    prefix="${BASH_REMATCH[5]}"
                    eval "$6"
                    valid_found=1   # Trigger valid private IPs found
                else
                    network="${line%.*}"
                    prefix="${BASH_REMATCH[5]}"
                    eval "$7"
                    valid_found=1   # Trigger valid private IPs found
                fi
            else
                eval "$8"
                non_valid_found=2   # Trigger non-valid IPs found
            fi
        elif [[ $line =~ $priv_network_172 ]]; then
            if [[ ${BASH_REMATCH[3]} -le 255 &&
            ${BASH_REMATCH[4]} -le 255 ]]; then
                if [[ ${BASH_REMATCH[5]} -eq 16 ]]; then
                    network="${line%.*.*}"
                    prefix="${BASH_REMATCH[5]}"
                    eval "${9}"
                    valid_found=1   # Trigger valid private IPs found
                else
                    network="${line%.*}"
                    prefix="${BASH_REMATCH[4]}"
                    eval "${10}"
                    valid_found=1   # Trigger valid private IPs found
                fi
            else
                eval "${11}"
                non_valid_found=2   # Trigger non-valid IPs found
            fi
        elif [[ $line =~ $priv_network_192 ]]; then
            if [[ ${BASH_REMATCH[2]} -le 255 &&
            ${BASH_REMATCH[3]} -le 255 ]]; then
                if [[ ${BASH_REMATCH[4]} -eq 16 ]]; then
                    network="${line%.*.*}"
                    prefix="${BASH_REMATCH[4]}"
                    eval "${12}"
                    valid_found=1   # Trigger valid public IPs found
                else
                    network="${line%.*}"
                    prefix="${BASH_REMATCH[4]}"
                    eval "${13}"
                    valid_found=1   # Trigger valid public IPs found
                fi
            else
                eval "${14}"
                non_valid_found=2   # Trigger non-valid IPs found
            fi
        fi
    done < "$1"

    # Return triggers
    return_values=($valid_found $non_valid_found)
}

# Task 37 main function
task37() {
    echo
    # Check user input: file
    check_file "$1"

    # Message with static line
    message_with_animated_or_static_bar \
        "File '$1' contents:" 55 - 0
    cat "$1"
    echo

    # Task 37 options
    if [ -z "$2" ]; then
        # Message with static line
        message_with_animated_or_static_bar \
            "Valid IP addresses from the file '$1':" 55 - 0

        # Validating IP addresses
        task37_silent_or_verbose "$1" 'echo $line' 'echo $line' ':' \
        'echo ${BASH_REMATCH[1]}' 'echo ${BASH_REMATCH[1]}' 'echo ${BASH_REMATCH[1]}' \
        ':' 'echo ${BASH_REMATCH[1]}' 'echo ${BASH_REMATCH[1]}' ':' \
        'echo ${BASH_REMATCH[1]}' 'echo ${BASH_REMATCH[1]}' ':'

        # Get the return value from the task37_silent_or_verbose function
        # Output if no valid IPs found
        no_found "${return_values[0]}" \
            'echo "No valid IP addresses found in the file '$1'."'
    elif [ "$2" == "-v" ]; then
        # Message with static line
        message_with_animated_or_static_bar \
            "Validating IP addresses from the file '$1':" 55 - 0

        # Validating IP addresses
        task37_silent_or_verbose "$1" 'echo "$line is a valid private IPv4 address"' \
        'echo "$line is a valid public IPv4 address"' \
        'echo "$line is not a valid IPv4 address"' \
        'echo "${BASH_REMATCH[1]} is a valid private IPv4 address, the network: ${line%.*.*.*}.0.0.0/${BASH_REMATCH[5]}"' \
        'echo "${BASH_REMATCH[1]} is a valid private IPv4 address, the network: ${line%.*.*}.0.0/${BASH_REMATCH[5]}"' \
        'echo "${BASH_REMATCH[1]} is a valid private IPv4 address, the network: ${line%.*}.0/${BASH_REMATCH[5]}"' \
        'echo "${BASH_REMATCH[1]} is not a valid IPv4 address"' \
        'echo "${BASH_REMATCH[1]} is a valid private IPv4 address, the network: ${line%.*.*}.0.0/${BASH_REMATCH[5]}"' \
        'echo "${BASH_REMATCH[1]} is a valid private IPv4 address, the network: ${line%.*}.0/${BASH_REMATCH[5]}"' \
        'echo "${BASH_REMATCH[1]} is not a valid IPv4 address"' \
        'echo "${BASH_REMATCH[1]} is a valid private IPv4 address, the network: ${line%.*.*}.0.0/${BASH_REMATCH[4]}"' \
        'echo "${BASH_REMATCH[1]} is a valid private IPv4 address, the network: ${line%.*}.0/${BASH_REMATCH[4]}"' \
        'echo "${BASH_REMATCH[1]} is not a valid IPv4 address"'

        # Get the return values from the task37_silent_or_verbose function
        # Output if no valid / non-valid IPs found
        no_found2 "${return_values[0]}" "${return_values[1]}" \
            'echo "No valid / non-valid IP addresses found in the file '$1'."'
    fi
}

# Task 38
# Task 38 temporary aux file vars
ping_file="/tmp/"$script_name".ping.active.ip"
nmap_file="/tmp/"$script_name".nmap.active.ip"
# Regexp vars to exclude default private networks and their broadcast addresses
private_broadcast_match='^((10\.255\.255\.255)|(172\.31\.255\.255)|(192\.168\.255\.255)|(255\.255\.255\.255))$'
private_network_match='^(10\.0\.0\.0|172\.16\.0\.0|192\.168\.0\.0)$'

# Task 38 aux function to check if 'nmap' utilitiy is installed
check_nmap() {
    local missing_util=false

    if ! check_util nmap; then
        echo "nmap utility not found."
        custom_install "nmap"
        missing_util=true
    fi
}

# Task 38 aux function to output based on temporary file check
check_temp_file_for_output() {
    if [ -f "$1" ]; then
        rm -rf "$1"
    elif [ "$2" == "0" ]; then
        eval "$3"
    else
        eval "$4"
    fi
}

# Task 38 aux function to ask user whether to proceed
ask_user() {
    echo "$1"
    tool=$
    while true; do
        read -r -p "Want to proceed? [y/n] " input
        case $input in
            [yY])
                break
                ;;
            [nN])
                echo "Exiting ..."
                exit
                ;;
            *)
                echo "Answer (y) for yes or (n) for no."
                ;;
        esac
    done
}

# Task 38 large IP range check warning aux function
check_ips_input_warning() {
    local contains_private_network=0  # Flag to track if any private network matches found

    for ip in "${@}"; do  # Use "${@}" to process all arguments as an array
        if [[ "$ip" =~ $priv_network_10 ]] ||
           [[ "$ip" =~ $priv_network_172 ]] ||
           [[ "$ip" =~ $priv_network_192 ]]; then
            contains_private_network=1
            break  # Exit the loop as we don't need to check further
        fi
    done

    if [ "${#}" -ge 100 ] && [ "${contains_private_network}" -eq 1 ]; then
        ask_user "WARNING: Scanning large IP lists and networks with "$1" can take a long time! "
    elif [ "${#}" -ge 100 ]; then
        ask_user "WARNING: Scanning large IP lists with "$1" can take a long time! "
    elif [ "${contains_private_network}" -eq 1 ]; then
        ask_user "WARNING: Scanning networks with "$1" can take a long time! "
    fi
}

# Task 38 aux function to scan /8 prefix network (scan8)
scan8() {
    if [[ ! $2 =~ "-a" ]]; then
        for ((octet2 = 1; octet2 <= 255; octet2++)); do
            for ((octet3 = 1; octet3 <= 255; octet3++)); do
                for ((octet4 = 1; octet4 < 255; octet4++)); do
                    if ping -c 1 -W 1 "$1.$octet2.$octet3.$octet4" >/dev/null; then
                        # Create temporary empty file for tracking active hosts
                        touch "$ping_file"
                        echo "$1.$octet2.$octet3.$octet4 is active" #>&2    # Override suppressing output with >&2
                    else
                        echo "$1.$octet2.$octet3.$octet4 is inactive" #>&2  # Override suppressing output by >&2
                    fi
                done
            done
        done
    else
        for ((octet2 = 1; octet2 <= 255; octet2++)); do
            for ((octet3 = 1; octet3 <= 255; octet3++)); do
                for ((octet4 = 1; octet4 < 255; octet4++)); do
                    if ping -c 1 -W 1 "$1.$octet2.$octet3.$octet4" >/dev/null; then
                        # Create temporary empty file for tracking active hosts
                        touch "$ping_file"
                        echo "$1.$octet2.$octet3.$octet4 is active" #>&2    # Override suppressing output with >&2
                    fi
                done
            done
        done
    fi
}

# Task 38 aux function to scan /16 prefix network (scan16)
scan16() {
    if [[ ! $2 =~ "-a" ]]; then
        for ((octet3 = 1; octet3 <= 255; octet3++)); do
            for ((octet4 = 1; octet4 < 255; octet4++)); do
                if ping -c 1 -W 1 "$1.$octet3.$octet4" >/dev/null; then
                    # Create temporary empty file for tracking active hosts
                    touch "$ping_file"
                    echo "$1.$octet3.$octet4 is active" #>&2    # Override suppressing output with >&2
                else
                    echo "$1.$octet3.$octet4 is inactive" #>&2  # Override suppressing output by >&2
                fi
            done
        done
    else
        for ((octet3 = 1; octet3 <= 255; octet3++)); do
            for ((octet4 = 1; octet4 < 255; octet4++)); do
                if ping -c 1 -W 1 "$1.$octet3.$octet4" >/dev/null; then
                    # Create temporary empty file for tracking active hosts
                    touch "$ping_file"
                    echo "$1.$octet3.$octet4 is active" #>&2    # Override suppressing output with >&2
                fi
            done
        done
    fi
}

# Task 38 aux function to scan /24 prefix network
scan24() {
    if [[ ! $2 =~ "-a" ]]; then
        for ((octet4 = 1; octet4 < 255; octet4++)); do
            if ping -c 1 -W 1 "$1.$octet4" >/dev/null; then
                # Create temporary empty file for tracking active hosts
                touch "$ping_file"
                echo "$1.$octet4 is active" #>&2    # Override suppressing output with >&2
            else
                echo "$1.$octet4 is inactive" #>&2  # Override suppressing output by >&2
            fi
        done
    else
        for ((octet4 = 1; octet4 < 255; octet4++)); do
            if ping -c 1 -W 1 "$1.$octet4" >/dev/null; then
                # Create temporary empty file for tracking active hosts
                touch "$ping_file"
                echo "$1.$octet4 is active" #>&2    # Override suppressing output with >&2
            fi
        done
    fi
}

# Task 38 aux function to scan /8 /16 /24 prefix networks with nmap
nmap_scan() {
    for ip in $(nmap -sn "$1" 2>/dev/null |
    grep -E [0-9]$ | cut -d ' ' -f5); do
        # Create temporary empty file for tracking active hosts
        touch "$nmap_file"
        echo "$ip is active"
    done
}

# Task 38 aux nmap function
nmap_active_or_inactive() {
    if [[ ! $1 =~ $private_broadcast_match ]] &&
       [[ ! $1 =~ $private_network_match ]] &&
       [[ ! $1 =~ "/" ]]; then
        if nmap -p 22 "$1" | grep -q 'is up'; then
            eval "$2"
            # Create temporary empty file for tracking active hosts
            touch "$nmap_file"
        else
            eval "$3"
        fi
    elif [[ $1 =~ $private_broadcast_match ]]; then
        eval "$4"
    else
        eval "$5"
    fi
}

# Task 38 aux ping function
ping_active_or_inactive() {
    if [[ ! $1 =~ $private_broadcast_match ]] &&
       [[ ! $1 =~ $private_network_match ]] &&
       [[ ! $1 =~ "/" ]]; then
        if ping -c 1 -W 1 "$1" >/dev/null; then
            eval "$2"
            # Create temporary empty file for tracking active hosts
            touch "$ping_file"
        else
            eval "$3"
        fi
    elif [[ $1 =~ $private_broadcast_match ]]; then
        eval "$4"
    else
        eval "$5"
    fi
}

# Task 38 main function
task38() {
    echo
    # Remove ping file if it exist after unexpected script exit
    if ! check_file "$ping_file" | grep -q 'not exist'; then
        rm -rf "$ping_file"
    fi

    # Check if the last argument is 'nmap' command
    if [[ "${@: -1}" =~ "nmap" ]]; then
        # Check if 'nmap' utilitiy is installed
        check_nmap

        # Warning if networks and/or large lists of IPs specified
        check_ips_input_warning "nmap" "$@"
        # Message with static line
        message_with_animated_or_static_bar \
            "Checking active / inactive hosts using 'nmap' command:" 55 - 0

        # task37_silent_or_verbose handles: $1 (for loop),
        # $2, $3, and $4 (skipped as ':')
        task37_silent_or_verbose <(for ip in "${@:1:$(($# - 1))}"; do
                                       echo "$ip";
                                   done) \
        'nmap_active_or_inactive $line "echo \"$line is active\"" \
        "echo \"$line is inactive\"" \
        "echo \"$line excluded as BROADCAST\"" \
        "echo \"$line is a NETWORK. Prefix needed (e.g. $line/24) to check hosts.\""' \
        'nmap_active_or_inactive $line "echo \"$line is active\"" \
        "echo \"$line is inactive\"" "echo \"$line excluded as BROADCAST\""' ':' \
        'nmap_scan $line' 'nmap_scan $line' 'nmap_scan $line' ':' \
        'nmap_scan $line' 'nmap_scan $line' ':' \
        'nmap_scan $line' 'nmap_scan $line' ':'

        # Check temporary file from the nmap_active_or_inactive function
        # Delete it if it was created
        # Output "No valid IPs provided." if IP validation fails
        check_temp_file_for_output "$nmap_file" "${return_values[0]}" \
            'echo "No valid IPs provided." && exit 1' \
            ':'
    # Active IPs only output using 'nmap' command
    elif [[ "${@: -1}" =~ "-a" ]] && [[ "${@: -2}" =~ "nmap" ]]; then
        # Warning if networks and/or large lists of IPs specified
        check_ips_input_warning "nmap" "$@"
        # Message with static line
        message_with_animated_or_static_bar \
            "Checking active hosts using 'nmap' command:" 55 - 0

        # task37_silent_or_verbose handles: $1 (for loop),
        # $2, $3, and $4 (skipped as ':')
        task37_silent_or_verbose <(for ip in "${@:1:$(($# - 2))}"; do
                                       echo "$ip";
                                   done) \
        'nmap_active_or_inactive $line "echo \"$line is active\"" ":" ":" ":"' \
        'nmap_active_or_inactive $line "echo \"$line is active\"" ":" ":"' ':' \
        'nmap_scan $line' 'nmap_scan $line' 'nmap_scan $line' ':' \
        'nmap_scan $line' 'nmap_scan $line' ':' \
        'nmap_scan $line' 'nmap_scan $line' ':'

        # Check temporary file from the nmap_active_or_inactive function
        # Output "No active hosts found." if no file was created
        # Output "No valid IPs provided." if IP validation fails
        check_temp_file_for_output "$nmap_file" "${return_values[0]}" \
            'echo "No valid IPs provided." && exit 1' \
            'echo "No active hosts found." && exit 1'
    else
        # Active IPs only output using 'ping' command
        if [[ "${@: -1}" =~ "-a" ]]; then
            # Warning if networks and/or large lists of IPs specified
            check_ips_input_warning "ping" "$@"
            active_only="${@: -1}"
            # Message with static line
            message_with_animated_or_static_bar \
                "Checking active hosts using 'ping' command:" 55 - 0

            # task37_silent_or_verbose handles: $1 (for loop),
            # $2, $3, and $4 (skipped as ':')
            task37_silent_or_verbose <(for ip in "${@:1:$(($# - 1))}"; do
                                           echo "$ip";
                                       done) \
            'ping_active_or_inactive $line "echo \"$line is active\"" ":" ":" ":"' \
            'ping_active_or_inactive $line "echo \"$line is active\"" ":"' ':' \
            'scan8 $network $active_only' 'scan16 $network $active_only' \
            'scan24 $network $active_only' ':' \
            'scan16 $network $active_only' 'scan24 $network $active_only' ':' \
            'scan16 $network $active_only' 'scan24 $network $active_only' ':'

            # Check temporary file from the ping_active_or_inactive function
            # Output "No active hosts found." if no file was created
            # Output "No valid IPs provided." if IP validation fails
            check_temp_file_for_output "$ping_file" "${return_values[0]}" \
                'echo "No valid IPs provided." && exit 1' \
                'echo "No active hosts found." && exit 1'
        else
            # Warning if networks and/or large lists of IPs specified
            check_ips_input_warning "ping" "$@"
            active_only="${@: -1}"
            # Message with static line
            message_with_animated_or_static_bar \
                "Checking active / inactive hosts using 'ping' command:" 55 - 0

            # task37_silent_or_verbose handles: $1 (for loop),
            # $2, $3, and $4 (skipped as ':')
            task37_silent_or_verbose <(for ip in "$@"; do
                                           echo "$ip";
                                       done) \
            'ping_active_or_inactive $line "echo \"$line is active\"" \
            "echo \"$line is inactive\"" "echo \"$line excluded as BROADCAST\"" \
            ":" ":" ":" \
            "echo \"$line is a NETWORK. Prefix needed (e.g. $line/24) to check hosts.\""' \
            'ping_active_or_inactive $line "echo \"$line is active\"" \
            "echo \"$line is inactive\"" \
            "echo \"$line excluded as BROADCAST\"" ":" ":" ":" ":"' ':' \
            'scan8 $network $active_only' 'scan16 $network $active_only' \
            'scan24 $network $active_only' ':' \
            'scan16 $network $active_only' 'scan24 $network $active_only' ':' \
            'scan16 $network $active_only' 'scan24 $network $active_only' ':'

            # Check temporary file from the ping_active_or_inactive function
            # Delete it if it was created
            # Output "No valid IPs provided." if IP validation fails
            check_temp_file_for_output "$ping_file" "${return_values[0]}" \
                'echo "No valid IPs provided." && exit 1' \
                ':'
        fi
    fi
}

# Task 39
task39() {
    echo
    local ips=$(echo $(./$script_name -t 36 pr) | grep -Eo "(${ipv4_match:1:-1})")

    if [ -z "$ips" ]; then
        # Message with static line
        message_with_animated_or_static_bar "No Private IP address assigned" 55 - 0
    else
        for ip in $ips; do
            interface=$(ip a | grep -v 'inet 127' |
            grep -E "inet $ip" | rev | cut -d ' ' -f 1 | rev)
            echo "$interface: $ip"
        done
    fi
}

# Task 40
task40() {
    echo
    # User input file path handling
    check_file "$1"

    local subdomains=$(openssl x509 -in "$1" -noout -text |
    grep -A 1 "X509v3 Subject Alternative Name:" | tail -n 1 |
    tr -d 'DNS:' | tr -s ' ' | cut -c2-)

    if [ -z "$subdomains" ]; then
        echo "No subdomains found in the certificate."
    else
        # Message with static line
        message_with_animated_or_static_bar \
            "Subdomains:" 55 - 0
        echo "$subdomains"
    fi
}

# Task 41
task41() {
    echo
    # User input file path handling
    check_file "$1"

    local filepath="$1"
    local filedir=$(dirname "$filepath")
    local filename=$(basename "$filepath")
    local fileext="${filename##*.}"
    local filename="${filename%.*}"

    # Message with static line
    message_with_animated_or_static_bar \
    "File info:" 55 - 0.01

    if [ "$fileext" = "$filename" ]; then
        echo "File Path: $filedir/"
        echo "File Name: $filename"
        echo "No File Extension"
    else
        echo "File Path: $filedir"
        echo "File Name: $filename"
        echo "File Extension: $fileext"
    fi
}

# Task 42
task42() {
    echo
    # Check user input: directory $1
    check_dir "$1"

    # File type user input handling: $2
    if [[ -z $(check_files "$1" "$2") ]]; then
        echo "No files matching '$2' found in the directory '$1'."
        exit 1
    fi

    # Dir info
    list_dir_or_file_info "$1"
    echo

    # Define regular expression pattern for size $3
    local pattern='^[+-]?[0-9]+([.][0-9]+)?[cwbkMG]$'

    # Check if size matches the pattern
    if [[ "$3" =~ $pattern ]]; then
        deleted_files=$(find "$1" -type f -name "$2" -size "$3" -delete -print)
        if [ -z "$deleted_files" ]; then
            echo "No files deleted. Check file size."
        else
            # Message with static line
            message_with_animated_or_static_bar \
            "Deleting files of type '$2' and size '$3' from the directory '$1':" \
            55 - 0
            echo "$deleted_files"
            echo
            # Dir info
            list_dir_or_file_info "$1"
        fi
    else
        echo "Size is invalid. Usage: -size [+/-]size[cwbkMG]"
    fi
}

# Task 43
task43() {
    echo
    # Check user input: file
    check_file "$1"

    # Check user input: create directory if it does not exist
    test_create_dir "$2"

    # Dir info
    list_dir_or_file_info "$2"
    echo

    # Generate a file / hash ID list for Task 43
    # Message with static line
    message_with_animated_or_static_bar \
    "Creating files with corresponding hash ID entry in the directory '$2':" 55 - 0.01

    while IFS=" " read -r file_name identifier; do
        printf "Creating file: %s with identifier: %s\n" \
        "$file_name" "$identifier"
        echo "$identifier" > "$2"/"$file_name"
        printf "File %s contents:\n" "$file_name"
        cat "$2"/"$file_name"
    done < "$1"

    # Dir info
    list_dir_or_file_info "$2"
    echo
}

### Task functions End #############################################################

### Demo Mode ######################################################################
# Check user input: directory
check_demodir() {
    # Check if the directory exists
    if [ -d "$1" ]; then
        echo "Directory '$1' exists. Specify a new one."
        exit 1
    fi
}

# Demo Mode aux function to generate a sample certificate for Task 40
generate_certificate() {
    # Create directory for Task 40
    test_create_dir "$1"

    # Generate .cnf file using a here document
    cat <<-EOL >"$2"
        [req]
        distinguished_name = req_distinguished_name
        req_extensions = v3_req

        [req_distinguished_name]
        CN = www.example.com

        [v3_req]
        subjectAltName = @alt_names

        [alt_names]
        DNS.1 = www.example.com
        DNS.2 = subdomain.example.com
        DNS.3 = example.org
EOL

    echo
    # Message with static line
    message_with_animated_or_static_bar \
    "Generating a sample SSL certificate for Task 40:" 55 - 0.01

    # Generate the certificate using openssl:
    # $1 - directory, $2 - .cnf file, $3 - private key, $4 - certificate
    openssl req -x509 -newkey rsa:2048 -keyout "$3" -out "$4" -days 365 \
    -nodes -subj "/C=US/O=Example Inc./CN=www.example.com" -extensions v3_req \
    -config "$2"
}

# Demo Mode aux file generator function for Task 42
file_generator() {
    for file in "${@:1:$#-1}"; do
        dd if=/dev/zero of="$file" bs=1024 count="${@:$#}"
    done
}

# Demo Mode aux file / hash ID list generator function for Task 43
file_list_generator() {
    while IFS=" " read -r id file; do
        echo $(basename "$file") "$id" >> "$1"/"$2"
    done < <(for all in "$1"/*.*; do
                 sha1sum $all;
             done)
}

### SandBox ########################################################################
# Progress bar animation
progress_bar() {
    sleep 2 &
    pid=$!
    frames="/ | \\ -"

    while kill -0 $pid >/dev/null 2>&1; do
        for frame in $frames; do
            printf "\r\033[1;32m$frame $1\e[0m"
            sleep 0.5
        done
    done
    printf "\n"
}

# Create SandBox Environment
init_sandbox() {
    # Environment Setup
    check_demodir "$1"
    curl -O https://gitlab.com/harrierpanels1/linux/-/raw/main/task01.tar.gz
    echo
    # Message with static line
    message_with_animated_or_static_bar \
        "The downloaded archive contents:" 55 - 0
    tar -tvzf task01.tar.gz
    mkdir "$1"
    tar -xzP -f task01.tar.gz -C "$1"
    tar -xzP -f task01.tar.gz -C . task01beta/dir3 task01beta/dir4
    rm task01.tar.gz

    progress_bar 'Initializing SandBox ...'
}

# Remove SandBox
rm_sandbox() {
    progress_bar 'Cleaning up SandBox ...'
    rm -rf "$1"
    rm -rf task01beta
}

### SandBox End ####################################################################

# Demo Mode main function
demo() {
    echo
    # SandBox Initiation
    init_sandbox "$1"

    # Environment adjustments
    # Create a file
    touch "$1"/task01/file
    # Get inode
    inode=$(find "$1"/task01 -maxdepth 1 -type f -printf '%i\n')

    # Generate a sample certificate for Task 40
    generate_certificate "$1"/task01/dir50 "$1"/task01/dir50/openssl.cnf \
    "$1"/task01/dir50/example.key "$1"/task01/dir50/example.crt

    echo
    # Generate files for Task 42
    # Message with static line
    message_with_animated_or_static_bar \
    "Generating files of size 4-16kB for Task 42:" 55 - 0.01

    # Create directory for Task 40
    test_create_dir "$1"/task01/dir55/dir1
    test_create_dir "$1"/task01/dir55/dir2
    # Generate files for Task 42
    echo
    # 4kB files
    file_generator "$1"/task01/dir55/no_delfile{1..3}.txt 4
    echo
    # 8kB files
    file_generator "$1"/task01/dir55/dir1/delfile{5..7}.txt 8
    echo
    file_generator "$1"/task01/dir55/dir2/delfile{8..10}.txt 8
    echo
    # 16kB files
    file_generator "$1"/task01/dir55/no_delfile{15..17}.txt 16
    echo

    # Generate a file / hash ID list for Task 43
    # Message with static line
    message_with_animated_or_static_bar \
    "Generating a file / hash ID list for Task 43:" 55 - 0.01

    file_list_generator "$1"/task01/dir55 list.txt
    list_dir_or_file_info "$1"/task01/dir55/list.txt
    echo

    # Task chain
    ./$script_name -t 1 "$1"/task01/file -t 2 "$1"/task01/dir $USER $USER \
        -t 3 "$1"/task01/dir -t 4 "$1"/task01/dir $USER -t 5 "$1"/task01/dir sh bash \
        -t 7 "$1"/task01/dir2 "$1"/task01/dir/file -t 8 "$1"/task01/dir2 "$1"/task01/dir/file \
        -t 9 $inode -t 10 $inode -t 11 "$1"/task01/dir/delete.txt \
        -t 12 "$1"/task01/dir3 '*.txt' 644 -t 13 "$1"/task01/dir3 "$1"/task01/dir4 \
        -t 14 -t 15 -t 16 -t 17 script.py "$1"/task01/dir4/script_soft.py \
        -t 19 cp "$1"/task01/dir6 "$1"/task01/dir7 \
        -t 19 rsync "$1"/task01/dir6/. "$1"/task01/dir8 \
        -t 20 cp "$1"/task01/dir6/. "$1"/task01/dir9 \
        -t 20 rsync "$1"/task01/dir6 "$1"/task01/dir10 \
        -t 21 cp "$1"/task01/dir11 "$1"/task01/dir12 \
        -t 21 rsync "$1"/task01/dir11/. "$1"/task01/dir13 -t 22 "$1"/task01/dir14 \
        -t 23 "$1"/task01/dir14 -t 24 "$1"/task01/dir15 \
        -t 27 "$1"/task01/dir6 "$1"/task01/dir20 -t 28 -t 29 -t 30 -t 31 -t 32 \
        -t 33 www.columbia.edu/~fdc/sample.html "$1"/task01/dir21 -t 34 a -t 34 b \
        -t 35 "$1"/task01/dir30 jpeg .txt -t 35 "$1"/task01/dir31 txt jpeg -t 36 pr \
        -t 36 pb -t 37 "$1"/task01/dir4/hosts-server.txt \
        -t 37 "$1"/task01/dir4/hosts-server.txt -v -t 39 \
        -t 40 "$1"/task01/dir50/example.crt -t 41 "$1"/task01/dir4/files/file2.txt \
        -t 42 "$1"/task01/dir55 '*.txt' 8192c \
        -t 43 "$1"/task01/dir55/list.txt "$1"/task01/dir77 \
        -t 6 "$1"/task01/dir "$1"/task01/dir2 cksum &&
    echo &&
    # Message with static line
    message_with_animated_or_static_bar "Proceeding with Task 18:" 55 - 0.01 &&
    ./$script_name -t 18 $HOME/"$1"/task01/dir4/hi.txt \
    "$1"/task01/dir3/za "$1"/task01/dir5/d/e &&
    ./$script_name -t 18 $(cat "$1"/task01/dir3/file_list.txt) "$1"/task01/dir5/d &&
    ls -1 "$1"/task01/dir4/files |
    xargs -I {} ./$script_name -t 18 "$1"/task01/dir4/files/{} "$1"/task01/dir5/d &&
    echo &&
    # Message with static line
    message_with_animated_or_static_bar "Proceeding with Task 25:" 55 - 0.01 &&
    ./$script_name -t 25 "$1"/task01/dir17/archive10.tgz "$1"/task01/dir16 0 --all &&
    ./$script_name -t 25 "$1"/task01/dir17/archive11.tlzma "$1"/task01/dir17 1 a &&
    ./$script_name -t 25 "$1"/task01/dir17/archive12.txz "$1"/task01/dir16 1 a/2.txt &&
    ./$script_name -t 25 "$1"/task01/dir17/archive1.tar "$1"/task01/dir17 0 3.txt \
        1.txt 4.txt a/2.txt &&
    ./$script_name -t 25 "$1"/task01/dir17/archive2.tar.gz "$1"/task01/dir16 0 3.txt a &&
    ./$script_name -t 25 "$1"/task01/dir17/archive3.tar.bz2 "$1"/task01/dir16 0 1.txt &&
    ./$script_name -t 25 "$1"/task01/dir17/archive4.tar.lz "$1"/task01/dir17 0 a/2.txt &&
    ./$script_name -t 25 "$1"/task01/dir17/archive5.tar.lzma "$1"/task01/dir16 1 --all &&
    ./$script_name -t 25 "$1"/task01/dir17/archive6.tar.xz "$1"/task01/dir17 0 1.txt &&
    ./$script_name -t 25 "$1"/task01/dir17/archive7.tar.Z "$1"/task01/dir16 0 a/2.txt &&
    ./$script_name -t 25 "$1"/task01/dir17/archive8.tlz "$1"/task01/dir17 0 1.txt \
        a/2.txt &&
    ./$script_name -t 25 "$1"/task01/dir17/archive9.tbz2 "$1"/task01/dir16 1 a/2.txt &&
    echo &&
    # Message with static line
    message_with_animated_or_static_bar "Proceeding with Task 26:" 55 - 0.01 &&
    ./$script_name -t 26 "$1"/task01/dir20/archive10.tgz "$1"/task01/dir \
        "$1"/task01/dir2 "$1"/task01/dir3 &&
    ./$script_name -t 26 ~/"$1"/task01/dir20/archive11.tlzma "$1"/task01/dir5 \
        "$1"/task01/dir4/hi.txt &&
    ./$script_name -t 26 "$1"/task01/dir20/archive12.txz "$1"/task01/dir3/hello.txt \
        "$1"/task01/dir5 "$1"/task01/dir2 &&
    ./$script_name -t 26 "$1"/task01/dir20/archive1.tar "$1"/task01/dir3/script2.py \
        "$1"/task01/dir2 "$1"/task01/dir5 &&
    ./$script_name -t 26 "$1"/task01/dir20/archive2.tar.gz "$1"/task01/dir/test.sh \
        "$1"/task01/dir/file2 "$1"/task01/dir4 &&
    ./$script_name -t 26 "$1"/task01/dir20/archive3.tar.bz2 "$1"/task01/dir18/sym.sh \
        "$1"/task01/dir18/dir2/.htaccess "$1"/task01/dir &&
    ./$script_name -t 26 "$1"/task01/dir20/archive4.tar.lz "$1"/task01/dir19/files \
        "$1"/task01/dir5 &&
    ./$script_name -t 26 "$1"/task01/dir20/archive5.tar.lzma \
        "$1"/task01/dir19/files/file1.txt "$1"/task01/dir16 &&
    ./$script_name -t 26 "$1"/task01/dir20/archive6.tar.xz \
        "$1"/task01/dir19/files/file2.txt "$1"/task01/dir &&
    ./$script_name -t 26 "$1"/task01/dir20/archive7.tar.Z \
        "$1"/task01/dir19/files/file3.txt "$1"/task01/dir2 &&
    ./$script_name -t 26 "$1"/task01/dir20/archive8.tlz "$1"/task01/dir19 \
        "$1"/task01/dir "$1"/task01/dir5 &&
    ./$script_name -t 26 "$1"/task01/dir20/archive9.tbz2 "$1"/task01/dir19 \
        "$1"/task01/dir18 &&
    echo &&
    # Message with static line
    message_with_animated_or_static_bar \
    "Task 38 examples: Ping multiple IPs/Networks passed from the command line:" \
    55 - 0.01 &&
    ./$script_name -t 38 8.0.1.0/24 10.255.255.255 221.1.1.2 1.1.1.1 192.168.0.{1..4} &&
    ./$script_name -t 38 255.255.255.255 172.16.0.0 221.1.1.2 1.1.1.1 -a &&
    echo &&
    # Message with static line
    message_with_animated_or_static_bar \
    "Task 38 examples: Nmap multiple IPs/Networks passed from the command line:" \
    55 - 0.01 &&
    ./$script_name -t 38 192.168.0.0/24 10.255.255.255 221.1.1.2 1.1.1.1 nmap &&
    echo &&
    # Message with static line
    message_with_animated_or_static_bar \
    "Task 38 examples: Checking hosts and networks by ping from a file (4-5 minutes):" \
    55 - 0.01 &&
    ./$script_name -t 38 $(cat "$1"/task01/dir4/hosts-server.txt) &&
    echo &&
    # Message with static line
    message_with_animated_or_static_bar \
    "Task 38 examples: Checking active hosts and networks by nmap from a file (fast):" \
    55 - 0.01 &&
    ./$script_name -t 38 $(cat "$1"/task01/dir4/hosts-server.txt) nmap -a &&
    echo &&
    # Message with static line
    message_with_animated_or_static_bar \
    "Task 38 examples: Scanning your local network by nmap (fast):" \
    55 - 0.01 &&
    ./$script_name -t 38 $(ip addr | grep 'inet ' |
    cut -d ' ' -f 6 | head -2 | tail -1) -nmap

    # SandBox Clean up
    rm_sandbox "$1"
}

### Demo Mode End ##################################################################

### Task drivers ###################################################################
if [ -z "$1" ]; then
    man
else
    while [[ "$#" -gt 0 ]]; do
        case $1 in
     -t|--task)
                shift
                task="$1"
                case $task in
                    1)
                        printf "\033[1;33mTask $task: List all system groups and retrieve their unique names and IDs. Save the output to a file.\e[0m"
                        shift
                        arg_check0 "$1"

                        task1 "$1"
                        ;;
                    2)
                        printf "\033[1;33mTask $task: Find all files and directories that have corresponding user and group permissions in a specified directory.\e[0m"
                        shift
                        arg_check3 "$#"

                        task2 "$@"
                        shift 2
                        ;;
                    3)
                        printf "\033[1;33mTask $task: Find all scripts in a specified directory and its subdirectories.\e[0m"
                        shift
                        arg_check0 "$1"

                        task3 "$1"
                        ;;
                    4)
                        printf "\033[1;33mTask $task: Search for script files (in a specified directory) by a specific user.\e[0m"
                        shift
                        arg_check2 "$#"

                        task4 "$@"
                        shift
                        ;;
                    5)
                        printf "\033[1;33mTask $task: Perform a recursive (case-sensitive) word or phrase search for a specific file type in a specified directory.\e[0m"
                        shift
                        arg_check3 "$#"

                        task5 "$@"
                        shift 2
                        ;;
                    6)
                        printf "\033[1;33m[x] Task $task: Find duplicate files in specified directories by size and hash function.\e[0m\n"
                        shift
                        arg_check2 "$#"

                        task6 "$@"
                        shift $(($# - 1))
                        ;;
                    7)
                        printf "\033[1;33mTask $task: Find all symbolic links to a file by its name and path.\e[0m"
                        shift
                        arg_check2 "$#"

                        task7 "$@"
                        shift
                        ;;
                    8)
                        printf "\033[1;33mTask $task: Find all hard links to a file by its name and path.\e[0m"
                        shift
                        arg_check2 "$#"

                        task8 "$@"
                        shift
                        ;;
                    9)
                        printf "\033[1;33mTask $task: Given only the inode of a file, find all its names.\e[0m"
                        shift
                        arg_check0 "$1"

                        task9 "$1"
                        ;;
                    10)
                        printf "\033[1;33mTask $task: Given only the inode of a file, find all its names (considering multiple mounted sections).\e[0m"
                        shift
                        arg_check0 "$1"

                        task10 "$1"
                        ;;
                    11)
                        printf "\033[1;33mTask $task: Correctly delete a file with symbolic or hard links if any.\e[0m"
                        shift
                        arg_check0 "$1"

                        task11 "$1"
                        ;;
                    12)
                        printf "\033[1;33mTask $task: Recursively change access rights to files (specified by file mask) in a specified directory.\e[0m"
                        shift
                        arg_check3 "$#"

                        task12 "$@"
                        shift 2
                        ;;
                    13)
                        printf "\033[1;33mTask $task: Compare two directories recursively and display only the files that differ.\e[0m"
                        shift
                        arg_check2 "$#"

                        task13 "$@"
                        shift
                        ;;
                    14)
                        printf "\033[1;33mTask $task: Get MAC addresses of network interfaces.\e[0m\n"

                        task14
                        ;;
                    15)
                        printf "\033[1;33mTask $task: Task $task: Display the list of users currently authorized in the system.\e[0m"

                        task15
                        ;;
                    16)
                        printf "\033[1;33mTask $task: Display a list of active network connections.\e[0m\n"

                        task16
                        ;;
                    17)
                        printf "\033[1;33mTask $task: Re-assign an existing symbolic link.\e[0m"
                        shift
                        arg_check2 "$#"

                        task17 "$@"
                        shift
                        ;;
                    18)
                        printf "\033[1;33m[x] Task $task: Create relative symbolic links in a specified directory to a list of files.\e[0m\n"
                        shift
                        arg_check2 "$#"

                        task18 "$@"
                        shift $(($# - 1))
                        ;;
                    19)
                        printf "\033[1;33mTask $task: Copy a directory containing soft and hard links, preserving ownership, permissions, and attributes for backup.\e[0m"
                        shift
                        arg_check3 "$#"

                        task19 "$@"
                        shift 2
                        ;;
                    20)
                        printf "\033[1;33mTask $task: Copy the directory containing hard and soft links to files, both direct and relative.\e[0m"
                        shift
                        arg_check3 "$#"

                        task20 "$@"
                        shift 2
                        ;;
                    21)
                        printf "\033[1;33mTask $task: Copy the directory contents while preserving permissions and attributes.\e[0m"
                        shift
                        arg_check3 "$#"

                        task21 "$@"
                        shift 2
                        ;;
                    22)
                        printf "\033[1;33mTask $task: In the project directory, convert all relative links to direct links.\e[0m"
                        shift
                        arg_check0 "$1"

                        task22 "$1"
                        ;;
                    23)
                        printf "\033[1;33mTask $task: In the project directory, convert all direct links to relative to the project directory.\e[0m"
                        shift
                        arg_check0 "$1"

                        task23 "$1"
                        ;;
                    24)
                        printf "\033[1;33mTask $task: Find all broken links in the specified directory and delete them.\e[0m"
                        shift
                        arg_check0 "$1"

                        task24 "$1"
                        ;;
                    25)
                        printf "\033[1;33m[x] Task $task: Unpack all contents or multiple directories or files from an archive to a specified location.\e[0m"
                        shift
                        arg_check4 "$#"

                        task25 "$@"
                        shift $(($# - 1))
                        ;;
                    26)
                        printf "\033[1;33m[x] Task $task: Pack the directory structure with files while preserving permissions and attributes. Multiple files and directories supported.\e[0m"
                        shift
                        arg_check2 "$#"

                        task26 "$@"
                        shift $(($# - 1))
                        ;;
                    27)
                        printf "\033[1;33mTask $task: Recursively copy the directory structure from the specified directory (excluding files).\e[0m"
                        shift
                        arg_check2 "$#"

                        task27 "$@"
                        shift
                        ;;
                    28)
                        printf "\033[1;33mTask $task: Display a list of all system users (names only) in alphabetical order.\e[0m"

                        task28
                        ;;
                    29)
                        printf "\033[1;33mTask $task: Display a list of all system users sorted by ID, in the format: login id.\e[0m"

                        task29
                        ;;
                    30)
                        printf "\033[1;33mTask $task: Display a list of all system users (names only) sorted by ID in reverse order.\e[0m"

                        task30
                        ;;
                    31)
                        printf "\033[1;33mTask $task: Display all users who do not have the right to log in or do not have a login shell in the system (two commands).\e[0m"

                        task31
                        ;;
                    32)
                        printf "\033[1;33mTask $task: Display all users who do not have the right to log in or do not have a login shell in the system (two commands).\e[0m"

                        task32
                        ;;
                    33)
                        printf "\033[1;33mTask $task: Download all links to href resources on a web page.\e[0m"
                        shift
                        arg_check2 "$#"

                        task33 "$@"
                        shift
                        ;;
                    34)
                        printf "\033[1;33mTask $task: Terminate processes that have been running for more than 5 days. Options: 'a' - killall; 'b' - no ps / killall.\e[0m"
                        shift
                        arg_check0 "$1"

                        task34 "$1"
                        ;;
                    35)
                        printf "\033[1;33mTask $task: Delete files with a certain extension if there is no corresponding file with a different extension in the same directory.\e[0m"
                        shift
                        arg_check3 "$#"

                        task35 "$@"
                        shift 2
                        ;;
                    36)
                        printf "\033[1;33mTask $task: Find your IP address using the command line. Options: 'pr' - Private IP; 'pb' - Public IP.\e[0m"
                        shift
                        arg_check0 "$1"

                        task36 "$1"
                        ;;
                    37)
                        printf "\033[1;33mTask $task: Get all IP addresses from a text file. Options: [-v] - verbosity.\e[0m"
                        shift
                        arg_check0 "$1"
                        case "$2" in
                            -v)
                                task37 "$@"
                                shift
                                ;;
                             *)
                                task37 "$1"
                                ;;
                        esac
                        ;;
                    38)
                        printf "\033[1;33m[x] Task $task: Check hosts from the command line input or file by ping or nmap utility.\e[0m"
                        shift
                        arg_check0 "$1"

                        task38 "$@"
                        shift $(($# - 1))
                        ;;
                    39)
                        printf "\033[1;33mTask $task: Filter the output of Task 36 to show uplink interfaces with assigned IPs.\e[0m"

                        task39
                        ;;
                    40)
                        printf "\033[1;33mTask $task: Get all subdomains from an SSL certificate.\e[0m"
                        shift
                        arg_check0 "$1"

                        task40 "$1"
                        ;;
                    41)
                        printf "\033[1;33mTask $task: Extract the path, name, and extension of a file in Bash from a string.\e[0m"
                        shift
                        arg_check0 "$1"

                        task41 "$1"
                        ;;
                    42)
                        printf "\033[1;33mTask $task: Delete files in a specified directory and its subdirectories based on their length and type.\e[0m"
                        shift
                        arg_check3 "$#"

                        task42 "$@"
                        shift 2
                        ;;
                    43)
                        printf "\033[1;33mTask $task: Create files in a designated directory using a file that contains file names and their corresponding hash IDs, and add the hash ID entry to each file.\e[0m"
                        shift
                        arg_check2 "$#"

                        task43 "$@"
                        shift
                        ;;
            -d|--demo)
                        printf "\033[1;33m[d] ======== Demo Mode ========\e[0m"
                        # Add your commands for Demo Mode here
                        shift
                        arg_check0 "$1"

                        demo "$1"
                        ;;
                    *)
                        echo "Invalid task number: $task"
                        ;;
                esac
                ;;
    -h|--help)
                man | less
                exit 0
                ;;
            *)
                echo "Invalid option: $1"
                man | head -n 4
                exit 1
                ;;
        esac
        shift
    done
fi
